//
//  NumberNode.h
//  MyGame01
//
//  Created by LiuHuanMing on 8/7/14.
//
//

#ifndef __MyGame01__NumberNode__
#define __MyGame01__NumberNode__

#include "cocos2d.h"

USING_NS_CC;

class NumberNode : public Node
{
public:
    NumberNode();
    ~NumberNode();
    virtual bool init();
    
public:
    CREATE_FUNC(NumberNode);
    
    static NumberNode* createNumberNodeStr(SpriteFrame* pSpriteFrame, const char *value, const char *numberString, int offset);
    static NumberNode* createNumberNode(SpriteFrame* pSpriteFrame, int value, const char *numberString, int offset);
    
private:
    Sprite* generateNumSprite(SpriteFrame* pSpriteFrame, char valueChar, const char* numberString);
    void generateNumNode(SpriteFrame* pSpriteFrame, const char* valueString,const char* numberString,int offset);
    
public:
    //  utility function for "com_number.plist"
    static NumberNode* createCOMNumberNode(const char *frameName, const char *text, const char *numberString, int offset);
    
};

#endif /* defined(__MyGame01__NumberNode__) */

