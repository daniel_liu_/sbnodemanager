//
//  NumberNode.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 8/7/14.
//
//

#include "NumberNode.h"

NumberNode::NumberNode()
{
}

NumberNode::~NumberNode()
{
}

bool NumberNode::init()
{
    if (Node::init()) {
        setCascadeOpacityEnabled(true);
        return true;
    }
    return false;
}

NumberNode* NumberNode::createNumberNodeStr(SpriteFrame* pSpriteFrame, const char *value, const char *numberString, int offset)
{
    NumberNode *numberNode = NumberNode::create();
    numberNode->generateNumNode(pSpriteFrame, value, numberString, offset);
    return numberNode;
}

NumberNode* NumberNode::createNumberNode(SpriteFrame* pSpriteFrame, int value, const char *numberString, int offset)
{
    char valueStr[64] = {0};
    sprintf(valueStr, "%d", value);
    return NumberNode::createNumberNodeStr(pSpriteFrame, valueStr, numberString, offset);
}

Sprite* NumberNode::generateNumSprite(SpriteFrame* pSpriteFrame, char valueChar, const char* numberString)
{
    Point offsetPoint = Point(pSpriteFrame->getRect().getMinX(), pSpriteFrame->getRect().getMinY());
    
    float offsetX     = offsetPoint.x;
    float offsetY     = offsetPoint.y;
    float frameWidth  = pSpriteFrame->getOriginalSize().width;
    float frameHeight = pSpriteFrame->getOriginalSize().height;
    
    int len = strlen(numberString);
    float dw = frameWidth / len;
    char* str = strchr(numberString, valueChar);
    if(str == NULL) CCLOG("createNum:valueChar=%c,numberString=%s", valueChar, numberString);
    CCAssert(str != NULL, "string error!");
    int count = (intptr_t)(str - numberString);
    
    Rect rect = Rect(offsetX + count * dw, offsetY, dw, frameHeight);
    return Sprite::createWithTexture(pSpriteFrame->getTexture(), rect);
}

void NumberNode::generateNumNode(SpriteFrame* pSpriteFrame, const char* valueString,const char* numberString,int offset)
{
    int count = strlen(valueString);
    Size size = pSpriteFrame->getOriginalSize();
    float dw = size.width / strlen(numberString);
    size.width = dw * count - (count - 1) * offset;
    setContentSize(size);
    setAnchorPoint(Point(0.5, 0.5));
    
    for (int i = 0; i < strlen(valueString); i++) {
        char valueChar = (char)valueString[i];
        Sprite* pSprite = generateNumSprite(pSpriteFrame, valueChar, numberString);
        pSprite->setPosition(Point((i + 0.5) * dw + i * offset, size.height * 0.5));
        addChild(pSprite);
    }
}

NumberNode* NumberNode::createCOMNumberNode(const char *frameName, const char *text, const char *numberString, int offset)
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("com_number.plist");
    SpriteFrame *sptFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(frameName);
    
    return NumberNode::createNumberNodeStr(sptFrame, text, numberString, offset);
}

