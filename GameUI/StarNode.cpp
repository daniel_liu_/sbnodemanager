//
//  StarNode.cpp
//  SBCodeGenerator
//
//  Created by LiuHuanMing on 7/26/14.
//
//

#include "StarNode.h"

StarNode::StarNode()
{
}

StarNode::~StarNode()
{
}

SEL_CallFuncN StarNode::onResolveCCBCCCallFuncSelector(Ref *pTarget, const char *pSelectorName)
{
	CCB_SELECTORRESOLVER_CALLFUNC_GLUE(this, "onActiveAnimDidStopped", StarNode::onActiveAnimDidStopped);
    
    return nullptr;
}

void StarNode::playActiveAnimation()
{
    runAnimationsNamed("active");
}

void StarNode::playInActiveAnimation()
{
    runAnimationsNamed("inactive");
}

void StarNode::onActiveAnimDidStopped(Node *pSender)
{
    CCLOG("on Active Anim Did Stopped !!!");
}

