//
//  BeginGameLayer.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/9/14.
//
//

#include "BeginGameLayer.h"

#include "SBMapNode.h"
#include "SBUtility.h"

BeginGameLayer::BeginGameLayer()
{
}

BeginGameLayer::~BeginGameLayer()
{
}

Control::Handler BeginGameLayer::onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName)
{
    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "onStartClicked", BeginGameLayer::onStartClicked);
    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "onCloseClicked", BeginGameLayer::onCloseClicked);
    
    return nullptr;
}

void BeginGameLayer::onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader)
{
    enableSwallow();
}

void BeginGameLayer::onStartClicked(Ref *pSender, Control::EventType pControlEvent)
{
    CCLOG("on start clicked in begin game layer.");
}

void BeginGameLayer::onCloseClicked(Ref *pSender, Control::EventType pControlEvent)
{
    dismissDialogFromUILayer(this);
}

