//
//  FriendListLayer.h
//  SBCodeGenerator
//
//  Created by Daniel.Liu on 7/26/14.
//
//

#ifndef __SBCodeGenerator__FriendListLayer__
#define __SBCodeGenerator__FriendListLayer__

#include "SBUINode.h"

class FriendListLayer : public SBUINode
, public TableViewDataSource
, public TableViewDelegate
{
public:
    FriendListLayer();
    ~FriendListLayer();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(FriendListLayer, create);
    SB_STATIC_CREATENODE2_METHOD(FriendListLayer, "friendlist.ccbi");
    
public:
    virtual void onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader);
    virtual bool onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode);
    virtual Control::Handler onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName);

public:
    virtual Size cellSizeForTable(TableView *table);
    virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(TableView *table);
    virtual void tableCellTouched(TableView* table, TableViewCell* cell);
    
private:
	void optionOneClicked(Ref *pSender, Control::EventType pControlEvent);
	void optionTwoClicked(Ref *pSender, Control::EventType pControlEvent);
	void onBackClicked(Ref *pSender, Control::EventType pControlEvent);
	void onInviteClicked(Ref *pSender, Control::EventType pControlEvent);
    
private:
    TableView*  m_pTableView;
    LayerColor* m_pContentLayer;
    CCButton*   m_pCloseButton;
    
};

CLASS_NODE_LOADER2(FriendListLayer);

#endif /* defined(__SBCodeGenerator__FriendListLayer__) */

