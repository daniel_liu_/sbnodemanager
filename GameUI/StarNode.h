//
//  StarNode.h
//  SBCodeGenerator
//
//  Created by Daniel.Liu on 7/26/14.
//
//

#ifndef __SBCodeGenerator__StarNode__
#define __SBCodeGenerator__StarNode__

#include "SBUINode.h"

class StarNode : public SBUINode
{
public:
    StarNode();
    ~StarNode();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(StarNode, create);
    SB_STATIC_CREATENODE2_METHOD(StarNode, "starnode.ccbi");
    
public:
    virtual SEL_CallFuncN onResolveCCBCCCallFuncSelector(Ref *pTarget, const char *pSelectorName);
    
public:
    void playActiveAnimation();
    void playInActiveAnimation();
    
private:
	void onActiveAnimDidStopped(Node *pSender);
    
};

CLASS_NODE_LOADER2(StarNode);

#endif /* defined(__SBCodeGenerator__StarNode__) */

