//
//  FriendListLayer.cpp
//  SBCodeGenerator
//
//  Created by LiuHuanMing on 7/26/14.
//
//

#include "FriendListLayer.h"

#include "FriendListCell.h"
#include "SBUtility.h"

FriendListLayer::FriendListLayer()
{
    CCLOG("FriendListLayer init!!!");
    
    m_pTableView = nullptr;
    m_pContentLayer = nullptr;
    m_pCloseButton = nullptr;
}

FriendListLayer::~FriendListLayer()
{
    CCLOG("FriendListLayer dealloc!!!");
    
    CC_SAFE_RELEASE(m_pContentLayer);
    CC_SAFE_RELEASE(m_pCloseButton);
}

void FriendListLayer::onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader)
{
    CCLOG("on node loaded on FriendListLayer!!!");
    enableSwallow();
    
    //  init table view
    Size size = m_pContentLayer->getContentSize();
    TableView *tableView = TableView::create(this, size);
    tableView->setDelegate(this);
    tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
    m_pContentLayer->addChild(tableView);
    m_pTableView = tableView;
}

bool FriendListLayer::onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode)
{
    SB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_pContentLayer", LayerColor*, m_pContentLayer);
    SB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_pCloseButton", CCButton*, m_pCloseButton);
    
    return false;
}

Control::Handler FriendListLayer::onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName)
{
	CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "optionOneClicked", FriendListLayer::optionOneClicked);
	CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "optionTwoClicked", FriendListLayer::optionTwoClicked);
	CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "onBackClicked", FriendListLayer::onBackClicked);
	CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "onInviteClicked", FriendListLayer::onInviteClicked);

    return nullptr;
}

Size FriendListLayer::cellSizeForTable(TableView *table)
{
    return Size(250, 55);
}

TableViewCell* FriendListLayer::tableCellAtIndex(TableView *table, ssize_t idx)
{
    TableViewCell *cell = (TableViewCell *)table->dequeueCell();
    
    if (!cell) {
        cell = new TableViewCell();
        cell->autorelease();
        
        FriendListCell *cellNode = FriendListCell::createNode2();
        cellNode->setTag(989801);
        cell->addChild(cellNode);
    }
    
    FriendListCell *cellNode = dynamic_cast<FriendListCell *>(cell->getChildByTag(989801));
    cellNode->updateUI();
    
    CCLOG("table cell at index: %zd", idx);
    
    return cell;
}

ssize_t FriendListLayer::numberOfCellsInTableView(TableView *table)
{
    return 21;
}

void FriendListLayer::tableCellTouched(TableView* table, TableViewCell* cell)
{
    CCLOG("touched cell %p, at index: %zd", cell, cell->getIdx());
}

void FriendListLayer::optionOneClicked(Ref *pSender, Control::EventType pControlEvent)
{
    runAnimationsNamed("tab0");
}

void FriendListLayer::optionTwoClicked(Ref *pSender, Control::EventType pControlEvent)
{
    runAnimationsNamed("tab1");
}

void FriendListLayer::onBackClicked(Ref *pSender, Control::EventType pControlEvent)
{
    dismissDialogFromUILayer(this);
}

void FriendListLayer::onInviteClicked(Ref *pSender, Control::EventType pControlEvent)
{
    CCLOG("onInviteClicked");
}

