//
//  BeginGameLayer.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/9/14.
//
//

#ifndef __MyGame01__BeginGameLayer__
#define __MyGame01__BeginGameLayer__

#include "SBUINode.h"

class SBMapNode;

class BeginGameLayer : public SBUINode
{
public:
    BeginGameLayer();
    ~BeginGameLayer();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(BeginGameLayer, create);
    SB_STATIC_CREATENODE_METHOD(BeginGameLayer, "begingame.ccbi");
    
public:
    virtual Control::Handler onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName);
    virtual void onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader);
    
private:
    void onStartClicked(Ref *pSender, Control::EventType pControlEvent);
    void onCloseClicked(Ref *pSender, Control::EventType pControlEvent);
};

CLASS_NODE_LOADER2(BeginGameLayer);

#endif /* defined(__MyGame01__BeginGameLayer__) */

