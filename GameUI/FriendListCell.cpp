//
//  FriendListCell.cpp
//  SBCodeGenerator
//
//  Created by LiuHuanMing on 7/26/14.
//
//

#include "FriendListCell.h"

FriendListCell::FriendListCell()
{
	m_pPlusButton = nullptr;
}

FriendListCell::~FriendListCell()
{
	CC_SAFE_RELEASE(m_pPlusButton);
}

bool FriendListCell::onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode)
{
	SB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_pPlusButton", CCButton*, m_pPlusButton);

    return false;
}

Control::Handler FriendListCell::onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName)
{
	CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "onPlusClicked", FriendListCell::onPlusClicked);

    return nullptr;
}

void FriendListCell::updateUI()
{
    CCLOG("debug update UI for FriendListCell");
}

void FriendListCell::onPlusClicked(Ref *pSender, Control::EventType pControlEvent)
{
    CCLOG("Plus Button Clicked on FriendListCell");
}

