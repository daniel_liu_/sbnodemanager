//
//  FriendListCell.h
//  SBCodeGenerator
//
//  Created by Daniel.Liu on 7/26/14.
//
//

#ifndef __SBCodeGenerator__FriendListCell__
#define __SBCodeGenerator__FriendListCell__

#include "SBUINode.h"

class FriendListCell : public SBUINode
{
public:
    FriendListCell();
    ~FriendListCell();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(FriendListCell, create);
    SB_STATIC_CREATENODE2_METHOD(FriendListCell, "friendcell.ccbi");
    
public:
    virtual bool onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode);
    virtual Control::Handler onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName);
    
public:
    void updateUI();
    
private:
	void onPlusClicked(Ref *pSender, Control::EventType pControlEvent);
    
private:
	CCButton*	m_pPlusButton;
};

CLASS_NODE_LOADER2(FriendListCell);

#endif /* defined(__SBCodeGenerator__FriendListCell__) */

