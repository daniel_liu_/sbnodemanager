//
//  SBUINode.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/2/14.
//
//

#ifndef __MyGame01__SBUINode__
#define __MyGame01__SBUINode__

#include "SBNodeManager.h"

class SBUINode : public Node
, public spritebuilder::CCBSelectorResolver
, public spritebuilder::CCBMemberVariableAssigner
, public spritebuilder::NodeLoaderListener
{
public:
    SBUINode();
    ~SBUINode();
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(SBUINode, create);
    
public:
    virtual SEL_MenuHandler  onResolveCCBCCMenuItemSelector(Ref *pTarget, const char *pSelectorName);
    virtual SEL_CallFuncN    onResolveCCBCCCallFuncSelector(Ref *pTarget, const char *pSelectorName);
    virtual Control::Handler onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName);
    
    virtual bool onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode);
    virtual bool onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue);
    
    virtual void onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader);
    
public:
    void setActionManager(CCBAnimationManager *pActionManager);
    void runAnimationsNamed(const string& animName);
    
public:
    void enableSwallow(bool enable_ = true);
    
protected:
    CCBAnimationManager*    m_pAniManager;
    
};

#endif /* defined(__MyGame01__SBUINode__) */

