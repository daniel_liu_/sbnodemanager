//
//  SBUtility.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/10/14.
//
//

#include "SBUtility.h"

#include "SBWorldRound.h"
#include "WorldIconNode.h"

#include "BeginGameLayer.h"
#include "StarNode.h"
#include "FriendListCell.h"
#include "FriendListLayer.h"

#include "SBUILayer.h"
#include "SBMapRound.h"
#include "TollgateIconNode.h"

#include "SBLoadingLayer.h"

Node* findUILayerParent(Node* child_)
{
    Node* parent = child_->getParent();
    if (parent != nullptr) {
        if (dynamic_cast<SBUILayer*>(parent)) {
            return parent;
        }else {
            return findUILayerParent(parent);
        }
    }
    return nullptr;
}

void dismissDialogFromUILayer(Node* dialogLayer)
{
    SBUILayer* uiLayer = dynamic_cast<SBUILayer*>(findUILayerParent(dialogLayer));
    if (uiLayer) {
        uiLayer->dismissDialog(dialogLayer);
    }
}

void registerLoaders()
{
    CCB_CLASS_REGISTER(SBWorldRound);
    CCB_CLASS_REGISTER(WorldIconNode);
    
    CCB_CLASS_REGISTER(BeginGameLayer);
    CCB_CLASS_REGISTER(StarNode);
    CCB_CLASS_REGISTER(FriendListCell);
    CCB_CLASS_REGISTER(FriendListLayer);
    
    CCB_CLASS_REGISTER(SBUILayer);
    CCB_CLASS_REGISTER(SBMapRound);
    CCB_CLASS_REGISTER(TollgateIconNode);
    
    CCB_CLASS_REGISTER(SBLoadingLayer);
}

void unregisterLoaders()
{
    CCB_CLASS_UNREGISTER(SBWorldRound);
    CCB_CLASS_UNREGISTER(WorldIconNode);
    
    CCB_CLASS_UNREGISTER(BeginGameLayer);
    CCB_CLASS_UNREGISTER(StarNode);
    CCB_CLASS_UNREGISTER(FriendListCell);
    CCB_CLASS_UNREGISTER(FriendListLayer);
    
    CCB_CLASS_UNREGISTER(SBUILayer);
    CCB_CLASS_UNREGISTER(SBMapRound);
    CCB_CLASS_UNREGISTER(TollgateIconNode);
    
    CCB_CLASS_UNREGISTER(SBLoadingLayer);
}

