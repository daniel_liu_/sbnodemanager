//
//  SBNodeManager.cpp
//  SBNodeManager
//
//  Created by LiuHuanMing on 7/26/14.
//
//

#include "SBNodeManager.h"

#define PHYSIC_FACTOR 32

static float FindPOTScale(float size, float fixedSize) {
	int scale = 1;
	while(fixedSize*scale < size) scale *= 2;
	return scale;
}

SBNodeManager* SBNodeManager::m_pSBNodeMgr = nullptr;

SBNodeManager* SBNodeManager::getInstance()
{
    if (!m_pSBNodeMgr)
    {
        do {
            m_pSBNodeMgr = new SBNodeManager();
            if (!m_pSBNodeMgr)
            {
                return nullptr;
            }
            return m_pSBNodeMgr;
        } while (0);
        
        delete m_pSBNodeMgr;
        return  nullptr;
    }
    
    return m_pSBNodeMgr;
}

void SBNodeManager::Release()
{
    delete m_pSBNodeMgr;
    m_pSBNodeMgr = nullptr;
}

SBNodeManager::SBNodeManager()
{
}

SBNodeManager::~SBNodeManager()
{
}

void SBNodeManager::configForSpriteBuilder()
{
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    
    //  resolution size
    Size size = director->getWinSizeInPixels();
    Size fixed = Size(size.width * 0.5, size.height * 0.5);
    // Find the minimal power-of-two scale that covers both the width and height.
    float scaleFactor = MIN(FindPOTScale(size.width, fixed.width), FindPOTScale(size.height, fixed.height));
    glview->setDesignResolutionSize(fixed.width, fixed.height, ResolutionPolicy::EXACT_FIT);
    director->setContentScaleFactor(scaleFactor);
    
    // add document search path
    char docPath[256] = {0};
    sprintf(docPath, "%s/Published-iOS", FileUtils::getInstance()->getWritablePath().c_str());
    FileUtils::getInstance()->addSearchPath(docPath, true);
    
    //  config resolution order & base search path
    spritebuilder::CCBReader::setupSpriteBuilder("resources-phonehd", PHYSIC_FACTOR);
}

Node* SBNodeManager::loadNodeFromCCBI(const char *pCCBFileName)
{
    return loadNodeFromCCBI(pCCBFileName,nullptr);
}

Node* SBNodeManager::loadNodeFromCCBI(const char *pCCBFileName,Node* pOwner)
{
    return loadNodeFromCCBI(pCCBFileName, pOwner, Director::getInstance()->getWinSize());
}

Node* SBNodeManager::loadNodeFromCCBI(const char *pCCBFileName, Node* pOwner,Size size)
{
    return loadNodeFromCCBI(pCCBFileName, pOwner, size, nullptr, nullptr);
}

Node* SBNodeManager::loadNodeFromCCBI(const char *pCCBFileName, Ref *target, SEL_CallFunc callbackFunc)
{
    return loadNodeFromCCBI(pCCBFileName, nullptr, Size(), target, callbackFunc);
}

Node* SBNodeManager::loadNodeFromCCBI(const char *pCCBFileName, Node* pOwner,Size size, Ref *target,SEL_CallFunc callbackFunc)
{
    return loadNodeFromCCBI(pCCBFileName, pOwner, size, target, callbackFunc, nullptr);
}

Node* SBNodeManager::loadNodeFromCCBI(const char *pCCBFileName,Node* pOwner,Size size, Ref *target,SEL_CallFunc callbackFunc,const char* seqName, spritebuilder::CCBAnimationManager** ppAnimationMgr)
{
    spritebuilder::CCBReader* pReader = new spritebuilder::CCBReader(GCC2DXNLLIB);
    string path = FileUtils::getInstance()->fullPathForFilename(pCCBFileName);
    
    auto pData = CCBCACHE->getDataForKey(pCCBFileName);
    if (pData.isNull()) {
        CCLOG("LOAD NODE FROM CCBI: %s", pCCBFileName);
        
        SBCCBCache *cache = new SBCCBCache();
        pData = cache->addCCBI(path);
    } else {
        CCLOG("[CACHED]LOAD NODE FROM CCBI: %s", pCCBFileName);
    }
    
    Node* pNode = pReader->readNodeGraphFromData(std::make_shared<Data>(pData), pOwner, size);
    
    spritebuilder::CCBAnimationManager* pAM = pReader->getAnimationManager();
    pAM->setAnimationCompletedCallback(target, callbackFunc);
    if (seqName) {
        pAM->runAnimationsForSequenceNamed(seqName);
    }
    if (ppAnimationMgr != nullptr) {
        CC_SAFE_RELEASE(*ppAnimationMgr);
        CC_SAFE_RETAIN(pAM);
        *ppAnimationMgr = pAM;
    }
    pReader->release();
    
    return pNode;
}

Node* SBNodeManager::loadNodeFromCCBI2(const char *pCCBFileName,spritebuilder::CCBAnimationManager** ppAnimationMgr)
{
    return loadNodeFromCCBI2(pCCBFileName, nullptr, ppAnimationMgr);
}

Node* SBNodeManager::loadNodeFromCCBI2(const char *pCCBFileName,Node* pOwner,spritebuilder::CCBAnimationManager** ppAnimationMgr)
{
    return loadNodeFromCCBI(pCCBFileName, pOwner, Director::getInstance()->getWinSize(), nullptr, nullptr, nullptr, ppAnimationMgr);
}

bool SBNodeManager::registerCCBLoader(const char * pClassName, NodeLoader * pNodeLoader)
{
    if (nullptr == GCC2DXNLLIB) {
        return false;
    }
    
    GCC2DXNLLIB->registerNodeLoader(pClassName, pNodeLoader);
    
    return true;
}

void SBNodeManager::unregisterCCBLoader(const char* pClassName)
{
    if (GCC2DXNLLIB) {
        GCC2DXNLLIB->unregisterNodeLoader(pClassName);
    }
}

