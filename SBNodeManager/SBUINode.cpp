//
//  SBUINode.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/2/14.
//
//

#include "SBUINode.h"

SBUINode::SBUINode()
{
    m_pAniManager = nullptr;
}

SBUINode::~SBUINode()
{
    CC_SAFE_RELEASE(m_pAniManager);
}

SEL_MenuHandler SBUINode::onResolveCCBCCMenuItemSelector(Ref *pTarget, const char *pSelectorName)
{
    return nullptr;
}

SEL_CallFuncN SBUINode::onResolveCCBCCCallFuncSelector(Ref *pTarget, const char *pSelectorName)
{
    return nullptr;
}

Control::Handler SBUINode::onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName)
{
    return nullptr;
}

bool SBUINode::onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode)
{
    return false;
}

bool SBUINode::onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue)
{
    return false;
}

void SBUINode::onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader)
{
}

void SBUINode::setActionManager(CCBAnimationManager *pActionManager)
{
    CC_SAFE_RELEASE(m_pAniManager);
    m_pAniManager = pActionManager;
    m_pAniManager->retain();
}

void SBUINode::runAnimationsNamed(const string& animName)
{
    if (m_pAniManager) {
        m_pAniManager->runAnimationsForSequenceNamed(animName.c_str());
    }
}

void SBUINode::enableSwallow(bool enable_)
{
    _eventDispatcher->removeEventListenersForTarget(this);
    
    //  touch event listener
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(enable_);
    listener->onTouchBegan = [](Touch* touch, Event* event) {
        return true;
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

