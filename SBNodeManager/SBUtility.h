//
//  SBUtility.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/10/14.
//
//

#ifndef __MyGame01__SBUtility__
#define __MyGame01__SBUtility__

#include "SBNodeManager.h"

Node* findUILayerParent(Node* child_);

void dismissDialogFromUILayer(Node* dialogLayer);

void registerLoaders();

void unregisterLoaders();

#endif /* defined(__MyGame01__SBUtility__) */
