//
//  SBNodeManager.h
//  SBNodeManager
//
//  Created by LiuHuanMing on 7/26/14.
//
//

#ifndef __SBNodeManager__SBNodeManager__
#define __SBNodeManager__SBNodeManager__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "spritebuilder/SpriteBuilder.h"

#include "SBCCBCache.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace spritebuilder;

class SBNodeManager
{
public:
    static SBNodeManager* getInstance();
    static void Release();
    
    SBNodeManager();
    ~SBNodeManager();
    
public:
    static void configForSpriteBuilder();
    
public:
    Node* loadNodeFromCCBI(const char *pCCBFileName);
    Node* loadNodeFromCCBI(const char *pCCBFileName, Node* pOwner);
    Node* loadNodeFromCCBI(const char *pCCBFileName, Node* pOwner, Size size);
    Node* loadNodeFromCCBI(const char *pCCBFileName, Ref *target, SEL_CallFunc callbackFunc);
    Node* loadNodeFromCCBI(const char *pCCBFileName, Node* pOwner, Size size, Ref* target, SEL_CallFunc callbackFunc);
    Node* loadNodeFromCCBI(const char *pCCBFileName, Node* pOwner, Size size, Ref* target, SEL_CallFunc callbackFunc, const char *seqName, spritebuilder::CCBAnimationManager** ppAnimationMgr = NULL);
    
    Node* loadNodeFromCCBI2(const char *pCCBFileName, spritebuilder::CCBAnimationManager** ppAnimationMgr);
    Node* loadNodeFromCCBI2(const char *pCCBFileName, Node* pOwner, spritebuilder::CCBAnimationManager** ppAnimationMgr);
    
public:
    bool registerCCBLoader(const char *pClassName, spritebuilder::NodeLoader *pCCNodeLoader);       //regist ccbi loader to UI
    void unregisterCCBLoader(const char *pClassName);
    
private:
    static SBNodeManager*   m_pSBNodeMgr;
    
};

#define SBNODEMGR       SBNodeManager::getInstance()
#define GCC2DXNLLIB     spritebuilder::NodeLoaderLibrary::getInstance()

#define SB_VIRTUAL_NEW_AUTORELEASE_CREATECCNODE_METHOD2(T) virtual T * createNode(cocos2d::Node * pParent, spritebuilder::CCBReader * ccbReader) { \
    spritebuilder::CCBAnimationManager *pAM = ccbReader->getAnimationManager(); \
    pAM->setAnimationCompletedCallback(NULL, NULL); \
    T *node_ = T::create(); \
    node_->setActionManager(pAM); \
    return node_; \
}

#define CLASS_LAYER_LOADER(className) class className##Loader : public spritebuilder::LayerLoader \
{ \
public: \
SB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(className##Loader, loader); \
protected: \
SB_VIRTUAL_NEW_AUTORELEASE_CREATECCNODE_METHOD(className); \
}

#define CLASS_LAYER_LOADER2(className) class className##Loader : public spritebuilder::LayerLoader \
{ \
public: \
SB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(className##Loader, loader); \
protected: \
SB_VIRTUAL_NEW_AUTORELEASE_CREATECCNODE_METHOD2(className); \
}

#define CLASS_NODE_LOADER(className) class className##Loader : public spritebuilder::NodeLoader \
{ \
public: \
SB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(className##Loader, loader); \
protected: \
SB_VIRTUAL_NEW_AUTORELEASE_CREATECCNODE_METHOD(className); \
}

#define CLASS_NODE_LOADER2(className) class className##Loader : public spritebuilder::NodeLoader \
{ \
public: \
SB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(className##Loader, loader); \
protected: \
SB_VIRTUAL_NEW_AUTORELEASE_CREATECCNODE_METHOD2(className); \
}

#define SB_STATIC_CREATENODE_METHOD(T, CCBISTR) static T * createNode() { \
    return dynamic_cast<T*>(SBNODEMGR->loadNodeFromCCBI(CCBISTR)); \
}

#define SB_STATIC_CREATENODE2_METHOD(T, CCBISTR) static T * createNode2() { \
    CCBAnimationManager *pActionManager = NULL; \
    T* node = dynamic_cast<T*>(SBNODEMGR->loadNodeFromCCBI2(CCBISTR, &pActionManager)); \
    node->setActionManager(pActionManager); \
    return node; \
}

#define CCB_CLASS_REGISTER(className)       SBNODEMGR->registerCCBLoader(#className,className##Loader::loader())
#define CCB_CLASS_UNREGISTER(className)     SBNODEMGR->unregisterCCBLoader(#className)

#endif /* defined(__SBNodeManager__SBNodeManager__) */

