//
//  SBCCBCache.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/1/14.
//
//

#ifndef __MyGame01__SBCCBCache__
#define __MyGame01__SBCCBCache__

#include <string>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <queue>
#include <string>
#include <unordered_map>
#include <functional>

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define CCBCACHE    SBCCBCache::getInstance()

class SBCCBCache : public Ref
{
public:
    static SBCCBCache * getInstance();
    static void destoryInstance();
    
public:
    SBCCBCache();
    virtual ~SBCCBCache();
    virtual std::string getDescription() const;
    
    Data addCCBI(const std::string &filepath);
    virtual void addCCBIAsync(const std::string &filepath, const std::function<void(Data)>& callback);
    virtual void unbindCCBIAsync(const std::string &filename);
    virtual void unbindAllCCBIAsync();
    
    Data getDataForKey(const std::string& key) const;
    bool reloadCCBI(const std::string& filename);
    
    void removeAllDatas();
    void removeDataForKey(const std::string &key);
    
    //wait for data cache to quit before destory instance
    //called by director, please do not called outside
    void waitForQuit();
    
private:
    void addDataAsyncCallBack(float dt);
    void loadData();
    
public:
    struct AsyncDataStruct
    {
    public:
        AsyncDataStruct(const std::string& fn, std::function<void(Data)> f) : filename(fn), callback(f) {}
        
        std::string filename;
        std::function<void(Data)> callback;
    };
    
protected:
    typedef struct _DataInfo
    {
        AsyncDataStruct *asyncDataStruct;
        Data            data;
    }DataInfo;
    
    std::thread* _loadingThread;
    
    std::queue<AsyncDataStruct*>* _asyncDataStructQueue;
    std::deque<DataInfo*>* _dataInfoQueue;
    
    std::mutex _asyncDataStructQueueMutex;
    std::mutex _dataInfoMutex;
    
    std::mutex _sleepMutex;
    std::condition_variable _sleepCondition;
    
    bool _needQuit;
    
    int _asyncRefCount;
    
    std::unordered_map<std::string, Data> _datas;
    
protected:
    static SBCCBCache* s_sharedCCBCache;
};

#endif /* defined(__MyGame01__SBCCBCache__) */

