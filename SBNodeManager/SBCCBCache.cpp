//
//  SBCCBCache.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/1/14.
//
//

#include "SBCCBCache.h"

#include <errno.h>
#include <stack>
#include <cctype>
#include <list>

#include "base/ccMacros.h"
#include "base/CCDirector.h"
#include "base/CCScheduler.h"
#include "platform/CCFileUtils.h"
#include "base/ccUtils.h"

#include "deprecated/CCString.h"

using namespace std;

SBCCBCache* SBCCBCache::s_sharedCCBCache = nullptr;

SBCCBCache * SBCCBCache::getInstance()
{
    if (s_sharedCCBCache == nullptr) {
        s_sharedCCBCache = new SBCCBCache();
    }
    return s_sharedCCBCache;
}

SBCCBCache::SBCCBCache()
: _loadingThread(nullptr)
, _asyncDataStructQueue(nullptr)
, _dataInfoQueue(nullptr)
, _needQuit(false)
, _asyncRefCount(0)
{
}

SBCCBCache::~SBCCBCache()
{
    CCLOGINFO("deallocing SBCCBCache: %p", this);
    
    for (auto it = _datas.begin(); it != _datas.end(); ++it)
        (it->second).clear();
    
    CC_SAFE_DELETE(_loadingThread);
}

void SBCCBCache::destoryInstance()
{
    if(s_sharedCCBCache)
    {
        s_sharedCCBCache->waitForQuit();
        CC_SAFE_DELETE(s_sharedCCBCache);
    }
}

std::string SBCCBCache::getDescription() const
{
    return StringUtils::format("<SBCCBCache | Number of datas = %d>", static_cast<int>(_datas.size()));
}

Data SBCCBCache::addCCBI(const std::string &path)
{
    Data data_;
    
    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(path);
    if (fullpath.size() == 0) return Data();
    
    auto it = _datas.find(fullpath);
    if (it != _datas.end()) data_ = it->second;
    
    if (data_.isNull())
    {
        data_ = FileUtils::getInstance()->getDataFromFile(fullpath);
        CCASSERT(data_.isNull() == false, StringUtils::format("Can not load file: %s", fullpath.c_str()).c_str());
        
        _datas.insert(std::make_pair(fullpath, data_));
    }
    
    return data_;
}

void SBCCBCache::addCCBIAsync(const std::string &path, const std::function<void (Data)> &callback)
{
    Data data_;
    
    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(path);
    
    auto it = _datas.find(fullpath);
    if (it != _datas.end()) data_ = it->second;
    
    if (!data_.isNull())
    {
        callback(data_);
        return;
    }
    
    //  lazy init
    if (_asyncDataStructQueue == nullptr)
    {
        _asyncDataStructQueue   = new queue<AsyncDataStruct*>();
        _dataInfoQueue          = new deque<DataInfo*>();
        
        //  create a new thread to load ccbi
        _loadingThread = new std::thread(&SBCCBCache::loadData, this);
        
        _needQuit = false;
    }
    
    if (0 == _asyncRefCount)
    {
        Director::getInstance()->getScheduler()->schedule(schedule_selector(SBCCBCache::addDataAsyncCallBack), this, 0, false);
    }
    
    ++_asyncRefCount;
    
    //  generate async struct
    AsyncDataStruct *dataStruct_ = new AsyncDataStruct(fullpath, callback);
    
    //  add async struct info queue
    _asyncDataStructQueueMutex.lock();
    _asyncDataStructQueue->push(dataStruct_);
    _asyncDataStructQueueMutex.unlock();
    
    _sleepCondition.notify_one();
}

void SBCCBCache::unbindCCBIAsync(const std::string& filename)
{
    _dataInfoMutex.lock();
    if (_dataInfoQueue && !_dataInfoQueue->empty())
    {
        std::string fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
        auto found = std::find_if(_dataInfoQueue->begin(), _dataInfoQueue->end(), [&fullpath](DataInfo* ptr)->bool{ return ptr->asyncDataStruct->filename == fullpath; });
        if (found != _dataInfoQueue->end())
        {
            (*found)->asyncDataStruct->callback = nullptr;
        }
    }
    _dataInfoMutex.unlock();
}

void SBCCBCache::unbindAllCCBIAsync()
{
    _dataInfoMutex.lock();
    if (_dataInfoQueue && !_dataInfoQueue->empty())
    {
        std::for_each(_dataInfoQueue->begin(), _dataInfoQueue->end(), [](DataInfo* ptr) { ptr->asyncDataStruct->callback = nullptr; });
    }
    _dataInfoMutex.unlock();
}

void SBCCBCache::loadData()
{
    AsyncDataStruct *asyncStruct = nullptr;
    
    while (true)
    {
        std::queue<AsyncDataStruct*> *pQueue = _asyncDataStructQueue;
        _asyncDataStructQueueMutex.lock();
        if (pQueue->empty())
        {
            _asyncDataStructQueueMutex.unlock();
            if (_needQuit) {
                break;
            }
            else {
                std::unique_lock<std::mutex> lk(_sleepMutex);
                _sleepCondition.wait(lk);
                continue;
            }
        }
        else
        {
            asyncStruct = pQueue->front();
            pQueue->pop();
            _asyncDataStructQueueMutex.unlock();
        }
        
        Data data_;
        bool generateData = false;
        
        auto it = _datas.find(asyncStruct->filename);
        if (it == _datas.end())
        {
            _dataInfoMutex.lock();
            DataInfo *dataInfo;
            size_t pos = 0;
            size_t infoSize = _dataInfoQueue->size();
            for (; pos < infoSize; pos++)
            {
                dataInfo = (*_dataInfoQueue)[pos];
                if (dataInfo->asyncDataStruct->filename.compare(asyncStruct->filename) == 0)
                    break;
            }
            _dataInfoMutex.unlock();
            if (infoSize == 0 || pos == infoSize)
                generateData = true;
        }
        
        if (generateData)
        {
            const std::string& filename = asyncStruct->filename;
            //  generate data
            data_ = FileUtils::getInstance()->getDataFromFile(filename);
            if (data_.isNull())
            {
                data_.clear();
                CCLOG("can not load %s", filename.c_str());
                continue;
            }
        }
        
        //  generate data info
        DataInfo *dataInfo = new DataInfo();
        dataInfo->asyncDataStruct = asyncStruct;
        dataInfo->data = data_;
        
        //  put the data info into the queue
        _dataInfoMutex.lock();
        _dataInfoQueue->push_back(dataInfo);
        _dataInfoMutex.unlock();
    }
    
    if (_asyncDataStructQueue != nullptr)
    {
        delete _asyncDataStructQueue;
        _asyncDataStructQueue = nullptr;
        delete _dataInfoQueue;
        _dataInfoQueue = nullptr;
    }
}

void SBCCBCache::addDataAsyncCallBack(float dt)
{
    //  the data is generated in loading thread
    std::deque<DataInfo*> *datasQueue = _dataInfoQueue;
    
    _dataInfoMutex.lock();
    if (datasQueue->empty())
    {
        _dataInfoMutex.unlock();
    }
    else
    {
        DataInfo *dataInfo = datasQueue->front();
        datasQueue->pop_front();
        _dataInfoMutex.unlock();
        
        AsyncDataStruct *asyncStruct = dataInfo->asyncDataStruct;
        Data data_ = dataInfo->data;
        
        const std::string& filename = asyncStruct->filename;
        
        if (!data_.isNull())
        {
            _datas.insert(std::make_pair(filename, data_));
        }
        else
        {
            auto it = _datas.find(filename);
            if (it != _datas.end())
                data_ = it->second;
        }
        
        if (asyncStruct->callback)
        {
            asyncStruct->callback(data_);
        }
        delete asyncStruct;
        delete dataInfo;
        
        --_asyncRefCount;
        if (0 == _asyncRefCount)
        {
            Director::getInstance()->getScheduler()->unschedule(schedule_selector(SBCCBCache::addDataAsyncCallBack), this);
        }
    }
}

bool SBCCBCache::reloadCCBI(const std::string &filename)
{
    Data data_;
    
    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
    if (fullpath.size() == 0)
    {
        return false;
    }
    
    auto it = _datas.find(fullpath);
    if (it != _datas.end()) {
        data_ = it->second;
    }
    
    bool ret = false;
    if (data_.isNull()) {
        data_ = this->addCCBI(fullpath);
        ret = (!data_.isNull());
    }
    
    return ret;
}

void SBCCBCache::removeAllDatas()
{
    for (auto it = _datas.begin(); it != _datas.end(); ++it) {
        (it->second).clear();
    }
    _datas.clear();
}

void SBCCBCache::removeDataForKey(const std::string &key)
{
    std::string key_ = key;
    auto it = _datas.find(key_);
    
    if (it == _datas.end()) {
        key_ = FileUtils::getInstance()->fullPathForFilename(key);
    }
    
    if (it != _datas.end()) {
        (it->second).clear();
        _datas.erase(it);
    }
}

Data SBCCBCache::getDataForKey(const std::string &keyName) const
{
    std::string key = keyName;
    auto it = _datas.find(key);
    
    if (it == _datas.end()) {
        key = FileUtils::getInstance()->fullPathForFilename(keyName);
        it = _datas.find(key);
    }
    
    if (it != _datas.end())
        return it->second;
    
    return Data();
}

void SBCCBCache::waitForQuit()
{
    //  notify sub thread to quick
    _needQuit = true;
    _sleepCondition.notify_one();
    if (_loadingThread) _loadingThread->join();
}

