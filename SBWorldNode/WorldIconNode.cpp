//
//  WorldIconNode.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/11/14.
//
//

#include "WorldIconNode.h"

WorldIconNode::WorldIconNode()
{
    m_bActive = false;
    
    m_iMapId = 0;
    m_pIconSprite = nullptr;
}

WorldIconNode::~WorldIconNode()
{
    CC_SAFE_RELEASE(m_pIconSprite);
}

void WorldIconNode::onEnterTransitionDidFinish()
{
    updateIconSprite();
    
    Node::onEnterTransitionDidFinish();
}

bool WorldIconNode::onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue)
{
    if (pTarget == this) {
        if (0 == strcmp(pMemberVariableName, "mapId")) { m_iMapId = pValue.asInt(); }
        if (0 == strcmp(pMemberVariableName, "iconSpriteframe")) { m_iconSptFrame = pValue.asString(); }
    }
    return true;
}

bool WorldIconNode::onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode)
{
    SB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_pIconSprite", Sprite*, m_pIconSprite);
    
    return true;
}

Control::Handler WorldIconNode::onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName)
{
    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "onMapIconClicked", WorldIconNode::onMapIconClicked);
    
    return nullptr;
}

void WorldIconNode::updateIconSprite()
{
    m_pIconSprite->setSpriteFrame(m_iconSptFrame.c_str());
    
    if (m_bActive == false) {
        m_pIconSprite->setColor(Color3B::GRAY);
    }else {
        m_pIconSprite->setColor(Color3B::WHITE);
    }
}

void WorldIconNode::onMapIconClicked(Ref* pSender, Control::EventType pControlEvent)
{
    if (m_iconDelegate) {
        m_iconDelegate->worldIconNodeClicked(this);
    }
}

