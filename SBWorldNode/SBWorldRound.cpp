//
//  SBWorldRound.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/11/14.
//
//

#include "SBWorldRound.h"

#include "SBWorldNode.h"
#include "SBMapNode.h"
#include "SBLoadingLayer.h"

#define TAG_WORLDICONNODENAME   "worldiconnode"

SBWorldRound::SBWorldRound()
{
    m_fRoundWidth = 0.0f;
    m_fRoundHeight = 0.0f;
}

SBWorldRound::~SBWorldRound()
{
    m_pWorldNodes.clear();
}

SBWorldRound* SBWorldRound::createWithName(string ccbName)
{
    return dynamic_cast<SBWorldRound*>(SBNODEMGR->loadNodeFromCCBI(ccbName.c_str()));
}

void SBWorldRound::onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader)
{
    reloadWorldNodes();
    
    //  active some world for test
    for (int i = 0; i < 2; i++) {
        WorldIconNode* iconNode = m_pWorldNodes.at(i);
        iconNode->setActive(true);
        iconNode->updateIconSprite();
    }
}

bool SBWorldRound::onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue)
{
    if (pTarget == this) {
        if (0 == strcmp(pMemberVariableName, "width")) { m_fRoundWidth = pValue.asFloat(); }
        if (0 == strcmp(pMemberVariableName, "height")) { m_fRoundHeight = pValue.asFloat(); }
    }
    return true;
}

#pragma mark - Moving Round Position

SBWorldNode* SBWorldRound::getWorldNode()
{
    SBWorldNode* worldNode = dynamic_cast<SBWorldNode*>(getParent());
    CCASSERT(worldNode, "SBWorldRound's parent must be a SBWorldNode!!!");
    return worldNode;
}

void SBWorldRound::setMapRoundPosX(float x)
{
    setPositionX(x);
}

void SBWorldRound::setMapRoundPosY(float y)
{
    float py = y;
    if (py > getMapRoundMaxYOffset())
    {
        py = getMapRoundMaxYOffset();
    }
    else if (py < getMapRoundMinYOffset())
    {
        py = getMapRoundMinYOffset();
    }
    
    setPositionY(py);
}

float SBWorldRound::getMapRoundMaxYOffset()
{
    return 0;
}

float SBWorldRound::getMapRoundMinYOffset()
{
    float winHeight = Director::getInstance()->getWinSize().height;
    return 0 - m_fRoundHeight + winHeight;
}

void SBWorldRound::move(float dy)
{
    float y = getPositionY() + dy;
    setMapRoundPosY(y);
}

#pragma mark - Tollgate Icon Nodes

void SBWorldRound::reloadWorldNodes()
{
    m_pWorldNodes.clear();
    
    Node* node_ = getChildByName(TAG_WORLDICONNODENAME);
    Vector<Node*> children_ = node_->getChildren();
    for (int i = 0; i < children_.size(); i++) {
        WorldIconNode* iconNode_ = dynamic_cast<WorldIconNode*>(children_.at(i));
        iconNode_->setWorldIconDelegate(this);
        m_pWorldNodes.insert(i, iconNode_);
    }
}

void SBWorldRound::worldIconNodeClicked(WorldIconNode* iconNode)
{
    if (iconNode) {
        CCLOG("clicked icon node for map id: %d [active: %d]", iconNode->getMapId(), iconNode->getActive());
        if (iconNode->getActive())
        {
            string ccbName = StringUtils::format("mapround_%d.ccbi", iconNode->getMapId());
            std::function<void(SBMapNode*)> callback_ = [](SBMapNode* pMapNode) {
                CCLOG("sbmap node loading callback here!!! map node: %p", pMapNode);
            };
            SBLoadingLayer* loadingLayer = SBMapNode::createLoadingLayer(callback_, ccbName.c_str());
            getWorldNode()->addChild(loadingLayer, E_ZORDER_WORLD_LOADING);
        }
    }
}

