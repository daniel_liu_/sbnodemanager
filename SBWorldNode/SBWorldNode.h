//
//  SBWorldNode.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/11/14.
//
//

#ifndef __MyGame01__SBWorldNode__
#define __MyGame01__SBWorldNode__

#include "SBUINode.h"

class SBWorldRound;
class SBUILayer;
class SBLoadingLayer;

#define kWorldRoundMoveThreshold    22.0f

enum {
    E_ZORDER_WORLDROUND     = -1,   // WorldRound Layer
    E_ZORDER_WORLD_UILAYER  = 1,    // UI Layer
    E_ZORDER_WORLD_LOADING  = 2,    // Loading Layer
}E_WORLD_ZORDER;

class SBWorldNode : public Node
{
public:
    SBWorldNode();
    ~SBWorldNode();
    
    virtual void onEnter();
    virtual void onExit();
    virtual void onEnterTransitionDidFinish();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(SBWorldNode, create);
    
    static Scene* createWorldNodeScene(const char* ccbName = "worldround.ccbi");
    static SBLoadingLayer* createLoadingLayer(std::function<void(SBWorldNode*)>& callback, const char* ccbName = "worldround.ccbi");
    
/**
 * Touch Dispatch
 */
public:
    void registerTouchDispatch();
    bool onTouchBegan(Touch *touch, Event *event);
    void onTouchMoved(Touch *touch, Event *event);
    void onTouchEnded(Touch *touch, Event *event);
    void onTouchCancelled(Touch *touch, Event *event);
    
    CC_SYNTHESIZE(bool, m_bCanTouch, CanTouch)
    CC_SYNTHESIZE(bool, m_bScroll, MapRoundScroll)    // for map round
    
private:
    bool m_bTouchScreen;
    Vec2 m_tTouchPoint;
    bool m_bDragging;
    bool m_bTouchMoved;
    Vec2 m_tScrollDistance;     // for MapRoundNode
    
/**
 * SBWorldRound
 */
public:
    SBWorldRound* getMapRound() { return m_pWorldRound; }
    
    void initWorldRound(string worldRoundName);
    void scrollingMapRound(float dt);
    
private:
    SBWorldRound* m_pWorldRound;    // ref
    
/**
 * SBUILayer
 */
public:
    SBUILayer* getUILayer() { return m_pUILayer; }
    
    void initUILayer();
    
private:
    SBUILayer* m_pUILayer;    // ref
    
};

#endif /* defined(__MyGame01__SBWorldNode__) */

