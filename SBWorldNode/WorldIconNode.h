//
//  WorldIconNode.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/11/14.
//
//

#ifndef __MyGame01__WorldIconNode__
#define __MyGame01__WorldIconNode__

#include "SBUINode.h"

class WorldIconNodeDelegate;

class WorldIconNode : public SBUINode
{
public:
    WorldIconNode();
    ~WorldIconNode();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(WorldIconNode, create);
    SB_STATIC_CREATENODE2_METHOD(WorldIconNode, "world_icon.ccbi");
    
    virtual void onEnterTransitionDidFinish();
    
    virtual bool onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue);
    virtual bool onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode);
    virtual Control::Handler onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName);
    
public:
    CC_SYNTHESIZE_PASS_BY_REF(bool, m_bActive, Active);     // active status
    
    CC_SYNTHESIZE(WorldIconNodeDelegate*, m_iconDelegate, WorldIconDelegate);
    
public:
    void updateIconSprite();
    
private:
    void onMapIconClicked(Ref* pSender, Control::EventType pControlEvent);
    
private:
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(int, m_iMapId, MapId);                   // ccbi property
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(string, m_iconSptFrame, IconSptFrame);   // ccbi property
    
    Sprite* m_pIconSprite;      // ccbi ref
    
};

CLASS_NODE_LOADER2(WorldIconNode);

class WorldIconNodeDelegate
{
public:
    virtual void worldIconNodeClicked(WorldIconNode* iconNode) {};
};

#endif /* defined(__MyGame01__WorldIconNode__) */
