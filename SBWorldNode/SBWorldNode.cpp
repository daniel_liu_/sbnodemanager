//
//  SBWorldNode.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/11/14.
//
//

#include "SBWorldNode.h"

#include "SBWorldRound.h"
#include "SBUILayer.h"
#include "SBLoadingLayer.h"
#include "SBTimeHelper.h"

SBWorldNode::SBWorldNode()
{
    m_bCanTouch = true;
    m_bScroll = false;
    
    m_bTouchScreen = false;
    m_tTouchPoint = Vec2();
    m_bDragging = false;
    m_bTouchMoved = false;
    m_tScrollDistance = Vec2();
    
    m_pWorldRound = nullptr;
    m_pUILayer = nullptr;
}

SBWorldNode::~SBWorldNode()
{
}

void SBWorldNode::onEnter()
{
    Node::onEnter();
}

void SBWorldNode::onExit()
{
    Director::getInstance()->getEventDispatcher()->removeEventListenersForTarget(this);
    
    Node::onExit();
}

void SBWorldNode::onEnterTransitionDidFinish()
{
    Node::onEnterTransitionDidFinish();
    
    //  touch
    registerTouchDispatch();
    
    //  UI
    initUILayer();
    getUILayer()->showHomeButton(false);
}

Scene* SBWorldNode::createWorldNodeScene(const char* ccbName)
{
    SBWorldNode* worldNode = SBWorldNode::create();
    worldNode->initWorldRound(ccbName);
    
    Scene* scene_ = Scene::create();
    scene_->addChild(worldNode);
    
    return scene_;
}

SBLoadingLayer* SBWorldNode::createLoadingLayer(std::function<void(SBWorldNode*)>& callback, const char* ccbName)
{
    SBLoadingLayer* loadingLayer = SBLoadingLayer::createNode();
    string copyName = string(ccbName);
    loadingLayer->onLoadStopped = [callback, copyName](SBLoadingLayer* pSender)
    {
        SBWorldNode* worldNode = SBWorldNode::create();
        worldNode->initWorldRound(copyName.c_str());
        
        Scene* worldScene = Scene::create();
        worldScene->addChild(worldNode);
        
        Director::getInstance()->replaceScene(worldScene);
        
        callback(worldNode);
    };
    
    SBLoadCCBITask *loadTask1 = new SBLoadCCBITask();
    loadTask1->addCCBIFile("worldround.ccbi");
    loadingLayer->addAsyncTask(loadTask1);
    
    //  load textures in the background(test with 10 times)
    for (int j = 0; j < 10; j++)
    {
        SBLoadTextureTask *pLoadTask1 = new SBLoadTextureTask();
        for (int i = 0; i < 10; i++) {
            char path_[64] = {0};
            sprintf(path_, "Resources/worldmap/map_%d.png", i);
            pLoadTask1->addTextrueFile(path_);
        }
        loadingLayer->addAsyncTask(pLoadTask1);
    }
    
    return loadingLayer;
}

#pragma mark - Touch Dispatch

void SBWorldNode::registerTouchDispatch()
{
    //  add event listener
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(false);
    listener->onTouchBegan = CC_CALLBACK_2(SBWorldNode::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(SBWorldNode::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(SBWorldNode::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(SBWorldNode::onTouchCancelled, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

bool SBWorldNode::onTouchBegan(Touch *touch, Event *event)
{
    if(!getCanTouch() || getMapRoundScroll() || m_bTouchScreen)
    {
        return false;
    }
    
    m_bTouchScreen = true;
    m_tTouchPoint = touch->getLocationInView();
    m_bTouchMoved = false;
    m_bDragging = true;         // dragging started
    m_tScrollDistance = Vec2();
    
    return true;
}

void SBWorldNode::onTouchMoved(Touch *touch, Event *event)
{
    if (m_bDragging)
    {
        Vec2 moveDistance, newPoint;
        newPoint = touch->getLocationInView();
        moveDistance = newPoint - m_tTouchPoint;
        m_tScrollDistance = moveDistance;
        
        float dy = moveDistance.y;
        m_pWorldRound->move(-dy);
        
        m_tTouchPoint = newPoint;
        m_bTouchMoved = true;
    }
}

void SBWorldNode::onTouchEnded(Touch *touch, Event *event)
{
    float dy = m_tScrollDistance.y;
    if (fabs(dy) >= kWorldRoundMoveThreshold) {
        schedule(schedule_selector(SBWorldNode::scrollingMapRound));
    }
    
    m_bDragging = false;
    m_bTouchMoved = false;
    m_bTouchScreen = false;
}

void SBWorldNode::onTouchCancelled(Touch *touch, Event *event)
{
    onTouchEnded(touch, event);
}

#pragma mark - SBWorldRound

void SBWorldNode::initWorldRound(string worldRoundName)
{
    SBWorldRound* worldRound = SBWorldRound::createWithName(worldRoundName.c_str());
    worldRound->setZOrder(E_ZORDER_WORLDROUND);
    addChild(worldRound);
    
    Size winSize = Director::getInstance()->getWinSize();
    worldRound->setAnchorPoint(Vec2(0.5, 0));
    worldRound->setPositionX(winSize.width * 0.5);
    
    m_pWorldRound = worldRound;
}

void SBWorldNode::scrollingMapRound(float dt)
{
    if (m_bDragging) {
        unschedule(schedule_selector(SBWorldNode::scrollingMapRound));
        return;
    }
    
    float dy = m_tScrollDistance.y;
    float newY;
    float minY = m_pWorldRound->getMapRoundMinYOffset();
    float maxY = m_pWorldRound->getMapRoundMaxYOffset();
    
    m_pWorldRound->move(-dy);
    
    newY = m_pWorldRound->getPositionY();
    
    m_tScrollDistance = m_tScrollDistance * 0.95;
    if ((fabsf(m_tScrollDistance.y) <= 1.0) || (newY >= maxY) || (newY <= minY)) {
        unschedule(schedule_selector(SBWorldNode::scrollingMapRound));
    }
}

#pragma mark - SBUILayer

void SBWorldNode::initUILayer()
{
    SBUILayer *uiLayer = SBUILayer::createNode();
    uiLayer->setZOrder(E_ZORDER_WORLD_UILAYER);
    addChild(uiLayer);
    
    m_pUILayer = uiLayer;
}

