//
//  SBWorldRound.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/11/14.
//
//

#ifndef __MyGame01__SBWorldRound__
#define __MyGame01__SBWorldRound__

#include "SBUINode.h"

#include "WorldIconNode.h"

class SBWorldNode;

class SBWorldRound
: public SBUINode
, public WorldIconNodeDelegate
{
public:
    SBWorldRound();
    ~SBWorldRound();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(SBWorldRound, create);
    
    static SBWorldRound* createWithName(string ccbName);
    
public:
    virtual void onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader);
    virtual bool onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue);
    
/**
 * Moving Round Position
 */
public:
    SBWorldNode* getWorldNode();
    
    void setMapRoundPosX(float x);
    void setMapRoundPosY(float y);
    float getMapRoundMaxYOffset();
    float getMapRoundMinYOffset();
    void move(float dy);
    
private:
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(float, m_fRoundWidth, RoundWidth);   // ccbi property
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(float, m_fRoundHeight, RoundHeight); // ccbi property
    
/**
 * World Icon Nodes
 */
public:
    void reloadWorldNodes();
    virtual void worldIconNodeClicked(WorldIconNode* iconNode);
    
private:
    Vector<WorldIconNode*>  m_pWorldNodes;
    
};

CLASS_NODE_LOADER2(SBWorldRound);

#endif /* defined(__MyGame01__SBWorldRound__) */
