//
//  SBHttpSprite.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/22/14.
//
//

#include "SBHttpSprite.h"

#include "SBImageDownloader.h"

bool SBHttpSprite::init()
{
    if (Sprite::init()) {
        return true;
    }
    return false;
}

SBHttpSprite::~SBHttpSprite()
{
    NotificationCenter::getInstance()->removeAllObservers(this);
}

SBHttpSprite* SBHttpSprite::create(const char * pszFileName)
{
    SBHttpSprite *pobSprite = new SBHttpSprite();
    if (pobSprite && pobSprite->initWithFile(pszFileName))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    
    return NULL;
}

void SBHttpSprite::startDownload(const char * imgUrl)
{
    SBImageDownloader *d = new SBImageDownloader();
    d->downloadImageFromUrl(imgUrl, this, callfuncO_selector(SBHttpSprite::imageDownloaded));
}

void SBHttpSprite::imageDownloaded(Ref *_dict)
{
    __Dictionary *dict = dynamic_cast<__Dictionary*>(_dict);
    const char * path = ((__String *)dict->objectForKey("kPath"))->getCString();
//    CCLOG(" === === end the path: %s  ===  ===", path);
    
    Texture2D *tx = SBImageDownloader::readSpriteFromPath(path);
    if (tx) {
        this->setTexture(tx);
    } else {
        CCLOG("can not load texture: %s", path);
    }
    
    NotificationCenter::getInstance()->removeObserver(this, ((__String *)dict->objectForKey("kNotifyName"))->getCString());
}

