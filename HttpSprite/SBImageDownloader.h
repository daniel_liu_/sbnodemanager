//
//  SBImageDownloader.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/22/14.
//
//

#ifndef __MyGame01__SBImageDownloader__
#define __MyGame01__SBImageDownloader__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "network/HttpClient.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace network;
using namespace std;

class SBImageDownloader : public Ref
{
public:
    CREATE_FUNC(SBImageDownloader);
    virtual bool init() { return true; };
    
    void downloadImageFromUrl(const char * url, Ref * pTarget, SEL_CallFuncO selector);
    static Texture2D * readSpriteFromPath(const char * path);
    
protected:
    void httpRequestComplete(HttpClient *sender, HttpResponse *response);
    std::string dumpHex(unsigned char* bin, int binLength);
    
};

#endif /* defined(__MyGame01__SBImageDownloader__) */

