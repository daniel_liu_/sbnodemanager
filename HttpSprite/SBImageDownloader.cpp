//
//  SBImageDownloader.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/22/14.
//
//

#include "SBImageDownloader.h"

#include "CCCrypto.h"

void SBImageDownloader::downloadImageFromUrl(const char * url, Ref * pTarget, SEL_CallFuncO selector)
{
    unsigned char buffer[CCCrypto::MD5_BUFFER_LENGTH];
    CCCrypto::MD5((void*)url, strlen(url), buffer);
    string str = dumpHex(buffer, CCCrypto::MD5_BUFFER_LENGTH);  // str is md5(url)
    
    //  add ccnotification
    if (pTarget && selector) {
        NotificationCenter::getInstance()->addObserver(pTarget, selector, str.c_str(), nullptr);
    }
    
    // detect cache
    string path = FileUtils::getInstance()->getWritablePath();
    path += str;
    bool exist_ = FileUtils::getInstance()->isFileExist(path);
    if (exist_) {
        //  post notification
        __Dictionary *dict = __Dictionary::create();
        dict->setObject(ccs(path.c_str()), "kPath");
        dict->setObject(ccs(str.c_str()), "kNotifyName");
        NotificationCenter::getInstance()->postNotification(str.c_str(), dict);
        return;
    }
    
    HttpRequest* request = new HttpRequest();
    request->setUrl(url);
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(this, httpresponse_selector(SBImageDownloader::httpRequestComplete));
    request->setTag(str.c_str());
    HttpClient::getInstance()->send(request);
    request->release();
}

Texture2D * SBImageDownloader::readSpriteFromPath(const char * path)
{
    return Director::getInstance()->getTextureCache()->addImage(path);
}

void SBImageDownloader::httpRequestComplete(HttpClient *sender, HttpResponse *response)
{
    if (!response) {
        CCLOG("response is null.");
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    CCLOG("response String: %s", statusString);
    
    if (!response->isSucceed())
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
    vector<char> *buffer = response->getResponseData();
    
    string path = FileUtils::getInstance()->getWritablePath();
    string bufffff(buffer->begin(),buffer->end());
    
    //保存到本地文件
    string md5name(response->getHttpRequest()->getTag());
    path += md5name;
    //    CCLOG("path: %s",path.c_str());
    FILE *fp = fopen(path.c_str(), "wb+");
    fwrite(bufffff.c_str(), 1,buffer->size(), fp);
    fclose(fp);
    
    //  post notification
    __Dictionary *dict = __Dictionary::create();
    dict->setObject(ccs(path.c_str()), "kPath");
    dict->setObject(ccs(md5name.c_str()), "kNotifyName");
    NotificationCenter::getInstance()->postNotification(md5name.c_str(), dict);
}

string SBImageDownloader::dumpHex(unsigned char* bin, int binLength)
{
    static const char* hextable = "0123456789abcdef";
    int strLength = binLength * 2 + 1;
    char str[strLength];
    memset(str, 0, strLength);
    int ci = 0;
    for (int i = 0; i < binLength; ++i)
    {
        unsigned char c = bin[i];
        str[ci++] = hextable[(c >> 4) & 0x0f];
        str[ci++] = hextable[c & 0x0f];
    }
    
    string string_object = str;
//    printf("md5: %s\n", string_object.c_str());
    return string_object;
}

