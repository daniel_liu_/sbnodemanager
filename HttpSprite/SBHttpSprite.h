//
//  SBHttpSprite.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/22/14.
//
//

#ifndef __MyGame01__SBHttpSprite__
#define __MyGame01__SBHttpSprite__

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

class SBHttpSprite : public Sprite
{
public:
    virtual bool init();
    ~SBHttpSprite();
    
    static SBHttpSprite* create(const char * pszFileName);
    void startDownload(const char * imgUrl);
    
protected:
    void imageDownloaded(Ref *dict);        //  notification
    
};

#endif /* defined(__MyGame01__SBHttpSprite__) */

