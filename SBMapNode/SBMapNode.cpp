//
//  SBMapNode.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/4/14.
//
//

#include "SBMapNode.h"

#include "SBMapRound.h"
#include "SBUILayer.h"
#include "SBLoadingLayer.h"

SBMapNode::SBMapNode()
{
    m_bCanTouch = true;
    m_bScroll = false;
    
    m_bTouchScreen = false;
    m_tTouchPoint = Vec2();
    m_bDragging = false;
    m_bTouchMoved = false;
    m_tScrollDistance = Vec2();
    
    m_pMapRound = nullptr;
    m_pUILayer = nullptr;
}

SBMapNode::~SBMapNode()
{
}

void SBMapNode::onEnter()
{
    Node::onEnter();
}

void SBMapNode::onExit()
{
    Director::getInstance()->getEventDispatcher()->removeEventListenersForTarget(this);
    
    Node::onExit();
}

void SBMapNode::onEnterTransitionDidFinish()
{
    Node::onEnterTransitionDidFinish();
    
    //  touch
    registerTouchDispatch();
    
    //  UI
    initUILayer();
}

Scene* SBMapNode::createMapNodeScene(const char* ccbName)
{
    SBMapNode* mapNode = SBMapNode::create();
    mapNode->initMapRound(ccbName);
    
    Scene* scene_ = Scene::create();
    scene_->addChild(mapNode);
    
    return scene_;
}

SBLoadingLayer* SBMapNode::createLoadingLayer(std::function<void(SBMapNode*)>& callback, const char* ccbName)
{
    SBLoadingLayer* loadingLayer = SBLoadingLayer::createNode();
    string copyName = string(ccbName);
    loadingLayer->onLoadStopped = [callback, copyName](SBLoadingLayer* pSender)
    {
        SBMapNode* mapNode = SBMapNode::create();
        mapNode->initMapRound(copyName.c_str());
        
        Scene* mapScene = Scene::create();
        mapScene->addChild(mapNode);
        
        Director::getInstance()->replaceScene(mapScene);
        
        callback(mapNode);
    };
    
    SBLoadCCBITask *loadTask1 = new SBLoadCCBITask();
    loadTask1->addCCBIFile(ccbName);
    loadingLayer->addAsyncTask(loadTask1);
    
    return loadingLayer;
}

#pragma mark - Touch Dispatch

void SBMapNode::registerTouchDispatch()
{
    //  add event listener
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(false);
    listener->onTouchBegan = CC_CALLBACK_2(SBMapNode::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(SBMapNode::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(SBMapNode::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(SBMapNode::onTouchCancelled, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

bool SBMapNode::onTouchBegan(Touch *touch, Event *event)
{
    if(!getCanTouch() || getMapRoundScroll() || m_bTouchScreen)
    {
        return false;
    }
    
    m_bTouchScreen = true;
    m_tTouchPoint = touch->getLocationInView();
    m_bTouchMoved = false;
    m_bDragging = true;         // dragging started
    m_tScrollDistance = Vec2();
    
    return true;
}

void SBMapNode::onTouchMoved(Touch *touch, Event *event)
{
    if (m_bDragging)
    {
        Vec2 moveDistance, newPoint;
        newPoint = touch->getLocationInView();
        moveDistance = newPoint - m_tTouchPoint;
        m_tScrollDistance = moveDistance;
        
        float dy = moveDistance.y;
        m_pMapRound->move(-dy);
        
        m_tTouchPoint = newPoint;
        m_bTouchMoved = true;
    }
}

void SBMapNode::onTouchEnded(Touch *touch, Event *event)
{
    float dy = m_tScrollDistance.y;
    if (fabs(dy) >= kMapRoundMoveThreshold) {
        schedule(schedule_selector(SBMapNode::scrollingMapRound));
    }
    
    m_bDragging = false;
    m_bTouchMoved = false;
    m_bTouchScreen = false;
}

void SBMapNode::onTouchCancelled(Touch *touch, Event *event)
{
    onTouchEnded(touch, event);
}

#pragma mark - SBMapRound

void SBMapNode::initMapRound(string mapRoundName)
{
    SBMapRound* mapRound = SBMapRound::createWithName(mapRoundName.c_str());
    mapRound->setZOrder(E_ZORDER_MAP_ROUND);
    addChild(mapRound);
    
    Size winSize = Director::getInstance()->getWinSize();
    mapRound->setAnchorPoint(Vec2(0.5, 0));
    mapRound->setPositionX(winSize.width * 0.5);
    
    m_pMapRound = mapRound;
}

void SBMapNode::scrollingMapRound(float dt)
{
    if (m_bDragging) {
        unschedule(schedule_selector(SBMapNode::scrollingMapRound));
        return;
    }
    
    float dy = m_tScrollDistance.y;
    float newY;
    float minY = m_pMapRound->getMapRoundMinYOffset();
    float maxY = m_pMapRound->getMapRoundMaxYOffset();
    
    m_pMapRound->move(-dy);
    
    newY = m_pMapRound->getPositionY();
    
    m_tScrollDistance = m_tScrollDistance * 0.95;
    if ((fabsf(m_tScrollDistance.y) <= 1.0) || (newY >= maxY) || (newY <= minY)) {
        unschedule(schedule_selector(SBMapNode::scrollingMapRound));
    }
}

#pragma mark - SBUILayer

void SBMapNode::initUILayer()
{
    SBUILayer *uiLayer = SBUILayer::createNode();
    uiLayer->setZOrder(E_ZORDER_UILAYER);
    addChild(uiLayer);
    
    m_pUILayer = uiLayer;
}

