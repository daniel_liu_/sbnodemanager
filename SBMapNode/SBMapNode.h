//
//  SBMapNode.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/4/14.
//
//

#ifndef __MyGame01__SBMapNode__
#define __MyGame01__SBMapNode__

#include "SBUINode.h"

class SBMapRound;
class SBUILayer;
class SBLoadingLayer;

#define kMapRoundMoveThreshold  22.0f

enum {
    E_ZORDER_MAP_ROUND  = -1,   // MapRound Layer
    E_ZORDER_UILAYER    = 1,    // UI Layer
}E_MAP_ZORDER;

class SBMapNode : public Node
{
public:
    SBMapNode();
    ~SBMapNode();
    
    virtual void onEnter();
    virtual void onExit();
    virtual void onEnterTransitionDidFinish();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(SBMapNode, create);
    
    static Scene* createMapNodeScene(const char* ccbName);
    static SBLoadingLayer* createLoadingLayer(std::function<void(SBMapNode*)>& callback, const char* ccbName);
    
/**
 * Touch Dispatch
 */
public:
    void registerTouchDispatch();
    bool onTouchBegan(Touch *touch, Event *event);
    void onTouchMoved(Touch *touch, Event *event);
    void onTouchEnded(Touch *touch, Event *event);
    void onTouchCancelled(Touch *touch, Event *event);
    
    CC_SYNTHESIZE(bool, m_bCanTouch, CanTouch)
    CC_SYNTHESIZE(bool, m_bScroll, MapRoundScroll)    // for map round
    
private:
    bool m_bTouchScreen;
    Vec2 m_tTouchPoint;
    bool m_bDragging;
    bool m_bTouchMoved;
    Vec2 m_tScrollDistance;     // for MapRoundNode
    
/**
 * SBMapRound
 */
public:
    SBMapRound* getMapRound() { return m_pMapRound; }
    
    void initMapRound(string mapRoundName);
    void scrollingMapRound(float dt);
    
private:
    SBMapRound* m_pMapRound;    // ref
    
/**
 * SBUILayer
 */
public:
    SBUILayer* getUILayer() { return m_pUILayer; }
    
    void initUILayer();
    
private:
    SBUILayer* m_pUILayer;    // ref
    
};

#endif /* defined(__MyGame01__SBMapNode__) */

