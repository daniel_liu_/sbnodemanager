//
//  SBMapRound.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/4/14.
//
//

#ifndef __MyGame01__SBMapRound__
#define __MyGame01__SBMapRound__

/**
 * == IMPORTANT ==
 * ---------------
 * 1. add SBMapRound with setZOrder(-1)
 * 2. parent touch dispatch must be setSwallowTouches(false)
 * ---------------
 */

/**
 * == TODO ==
 * ---------------
 * 1. 视角移动到某个关卡(有动画，无动画)
 * ---------------
 */

#include "SBUINode.h"

#include "TollgateIconNode.h"

class SBMapNode;

class SBMapRound
: public SBUINode
, public TollgateIconNodeDelegate
{
public:
    SBMapRound();
    ~SBMapRound();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(SBMapRound, create);
    
    static SBMapRound* createWithName(string ccbName);
    
public:
    virtual void onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader);
    virtual bool onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue);
    
/**
 * Moving Round Position
 */
public:
    SBMapNode* getMapNode();
    
    void setMapRoundPosX(float x);
    void setMapRoundPosY(float y);
    float getMapRoundMaxYOffset();
    float getMapRoundMinYOffset();
    void move(float dy);
    
private:
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(float, m_fRoundWidth, RoundWidth);   // ccbi property
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(float, m_fRoundHeight, RoundHeight); // ccbi property

/**
 * Tollgate Icon Nodes
 */
public:
    void reloadTollgateNodes();
    virtual void tollgateIconNodeClicked(TollgateIconNode* iconNode);
    
private:
    Vector<TollgateIconNode*>   m_pTollgateNodes;
    
};

CLASS_NODE_LOADER2(SBMapRound);

#endif /* defined(__MyGame01__SBMapRound__) */

