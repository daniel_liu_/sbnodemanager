//
//  SBMapRound.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/4/14.
//
//

#include "SBMapRound.h"

#include "SBMapNode.h"
#include "SBUILayer.h"
#include "BeginGameLayer.h"

#define TAG_TOLLGATESNODENAME   "tollgateiconnode"

SBMapRound::SBMapRound()
{
    m_fRoundWidth = 0.0f;
    m_fRoundHeight = 0.0f;
}

SBMapRound::~SBMapRound()
{
    m_pTollgateNodes.clear();
}

SBMapRound* SBMapRound::createWithName(string ccbName)
{
    return dynamic_cast<SBMapRound*>(SBNODEMGR->loadNodeFromCCBI(ccbName.c_str()));
}

void SBMapRound::onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader)
{
    reloadTollgateNodes();
    
    //  active some tolglate for test
    for (int i = 0; i < 2; i++) {
        TollgateIconNode* iconNode = m_pTollgateNodes.at(i);
        iconNode->setActive(true);
        iconNode->setStarNum(2);
        iconNode->updateIconSprite();
    }
}

bool SBMapRound::onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue)
{
    if (pTarget == this) {
        if (0 == strcmp(pMemberVariableName, "width")) { m_fRoundWidth = pValue.asFloat(); }
        if (0 == strcmp(pMemberVariableName, "height")) { m_fRoundHeight = pValue.asFloat(); }
    }
    return true;
}

#pragma mark - Moving Round Position

SBMapNode* SBMapRound::getMapNode()
{
    SBMapNode* mapNode = dynamic_cast<SBMapNode*>(getParent());
    CCASSERT(mapNode, "SBMapRound's parent must be a SBMapNode!!!");
    return mapNode;
}

void SBMapRound::setMapRoundPosX(float x)
{
    setPositionX(x);
}

void SBMapRound::setMapRoundPosY(float y)
{
    float py = y;
    if (py > getMapRoundMaxYOffset())
    {
        py = getMapRoundMaxYOffset();
    }
    else if (py < getMapRoundMinYOffset())
    {
        py = getMapRoundMinYOffset();
    }
    
    setPositionY(py);
}

float SBMapRound::getMapRoundMaxYOffset()
{
    return 0;
}

float SBMapRound::getMapRoundMinYOffset()
{
    float winHeight = Director::getInstance()->getWinSize().height;
    return 0 - m_fRoundHeight + winHeight;
}

void SBMapRound::move(float dy)
{
    float y = getPositionY() + dy;
    setMapRoundPosY(y);
}

#pragma mark - Tollgate Icon Nodes

void SBMapRound::reloadTollgateNodes()
{
    m_pTollgateNodes.clear();
    
    Node* node_ = getChildByName(TAG_TOLLGATESNODENAME);
    Vector<Node*> children_ = node_->getChildren();
    for (int i = 0; i < children_.size(); i++) {
        TollgateIconNode* iconNode_ = dynamic_cast<TollgateIconNode*>(children_.at(i));
        iconNode_->setTollgateIconDelegate(this);
        m_pTollgateNodes.insert(i, iconNode_);
    }
}

void SBMapRound::tollgateIconNodeClicked(TollgateIconNode* iconNode)
{
    if (iconNode)
    {
        CCLOG("clicked icon node for tollgate id: %d", iconNode->getTollgateId());
        //  show dialog only actived
        if (iconNode->getActive()) {
            BeginGameLayer *bgLayer = BeginGameLayer::createNode();
            getMapNode()->getUILayer()->showDialog(bgLayer);
        }
    }
}

