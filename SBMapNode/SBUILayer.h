//
//  SBUILayer.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/4/14.
//
//

#ifndef __MyGame01__SBUILayer__
#define __MyGame01__SBUILayer__

#include "SBUINode.h"

class SBMapNode;

enum {
    E_ZORDER_CONTROL    = 0,    // Control Layer
    E_ZORDER_DIALOG     = 1,    // Dialog Layer
    E_ZORDER_ALERT      = 2,    // Alert Layer
}E_UI_ZORDER;

class SBUILayer : public SBUINode
{
public:
    SBUILayer();
    ~SBUILayer();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(SBUILayer, create);
    SB_STATIC_CREATENODE_METHOD(SBUILayer, "uilayer.ccbi");
    
public:
    virtual Control::Handler onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName);
    virtual bool onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode);
    virtual void onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader);
    
public:
    SBMapNode* getMapNode();
    
public:
    void showDialog(Node* dialogLayer);
    void showAlert(Node* alertLayer);
    void dismissDialog(Node* dialogLayer);
    void dismissAlert(Node* alertLayer);
    
public:
    void showHomeButton(bool isShow = true);
    
private:
    void checkVisibleControlLayer();
    
private:
    void onBookClicked(Ref *pSender, Control::EventType pControlEvent);
    void onHomeClicked(Ref *pSender, Control::EventType pControlEvent);
    
private:
    Node*   m_pControlLayer;
    Node*   m_pDialogLayer;
    Node*   m_pAlertLayer;
    
};

CLASS_NODE_LOADER2(SBUILayer);

#endif /* defined(__MyGame01__SBUILayer__) */

