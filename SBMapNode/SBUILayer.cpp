//
//  SBUILayer.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/4/14.
//
//

#include "SBUILayer.h"

#include "SBMapNode.h"
#include "FriendListLayer.h"
#include "SBWorldNode.h"
#include "SBLoadingLayer.h"

SBUILayer::SBUILayer()
{
    m_pControlLayer = nullptr;
    m_pDialogLayer = nullptr;
    m_pAlertLayer = nullptr;
}

SBUILayer::~SBUILayer()
{
    CC_SAFE_RELEASE(m_pControlLayer);
    CC_SAFE_RELEASE(m_pDialogLayer);
    CC_SAFE_RELEASE(m_pAlertLayer);
}

Control::Handler SBUILayer::onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName)
{
    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "onBookClicked", SBUILayer::onBookClicked);
    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "onHomeClicked", SBUILayer::onHomeClicked);
    
    return nullptr;
}

bool SBUILayer::onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode)
{
    SB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_pControlLayer", Node*, m_pControlLayer);
    SB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_pDialogLayer",  Node*, m_pDialogLayer);
    SB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_pAlertLayer",   Node*, m_pAlertLayer);
    
    return true;
}

void SBUILayer::onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader)
{
    //  reorder layers
    m_pControlLayer->setZOrder(E_ZORDER_CONTROL);
    m_pDialogLayer->setZOrder(E_ZORDER_DIALOG);
    m_pAlertLayer->setZOrder(E_ZORDER_ALERT);
}

SBMapNode* SBUILayer::getMapNode()
{
    SBMapNode* mapNode = dynamic_cast<SBMapNode*>(getParent());
    CCASSERT(mapNode, "SBMapRound's parent must be a SBMapNode!!!");
    return mapNode;
}

void SBUILayer::showDialog(Node* dialogLayer)
{
    //  can add animation for the dialog layer
    
    m_pDialogLayer->addChild(dialogLayer);
    
    checkVisibleControlLayer();
}

void SBUILayer::showAlert(Node* alertLayer)
{
    //  can add animation for the alert layer
    
    m_pAlertLayer->addChild(alertLayer);
}

void SBUILayer::dismissDialog(Node* dialogLayer)
{
    //  can add remove animation for the dialog layer
    
    if (dialogLayer) {
        dialogLayer->removeFromParent();
    }
    
    checkVisibleControlLayer();
}

void SBUILayer::dismissAlert(Node* alertLayer)
{
    //  can add remove animation for the alert layer
    
    if (alertLayer) {
        alertLayer->removeFromParent();
    }
}

void SBUILayer::showHomeButton(bool isShow)
{
    CCButton* homeButton = m_pControlLayer->getChildByName<CCButton*>("homebutton");
    if (homeButton) {
        homeButton->setVisible(isShow);
    }
}

void SBUILayer::checkVisibleControlLayer()
{
    int dialogCount = (int)m_pDialogLayer->getChildrenCount();
    if (dialogCount > 0) { m_pControlLayer->setVisible(false); }
    else { m_pControlLayer->setVisible(true); }
}

void SBUILayer::onBookClicked(Ref *pSender, Control::EventType pControlEvent)
{
    FriendListLayer *listlayer = FriendListLayer::createNode2();
    showDialog(listlayer);
}

void SBUILayer::onHomeClicked(Ref *pSender, Control::EventType pControlEvent)
{
    removeAllChildren();
    
    std::function<void(SBWorldNode*)> callback_ = [](SBWorldNode* pWorldNode) {
        CCLOG("sbworld node loading callback here!!! world node: %p", pWorldNode);
    };
    SBLoadingLayer *loadingLayer = SBWorldNode::createLoadingLayer(callback_);
    addChild(loadingLayer);
}

