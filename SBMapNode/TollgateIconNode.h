//
//  TollgateIconNode.h
//  MyGame01
//
//  Created by LiuHuanMing on 9/5/14.
//
//

#ifndef __MyGame01__TollgateIconNode__
#define __MyGame01__TollgateIconNode__

#include "SBUINode.h"

class TollgateIconNodeDelegate;

class TollgateIconNode : public SBUINode
{
public:
    TollgateIconNode();
    ~TollgateIconNode();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(TollgateIconNode, create);
    SB_STATIC_CREATENODE2_METHOD(TollgateIconNode, "tollgate_icon.ccbi");
    
    virtual void onEnterTransitionDidFinish();
    
    virtual bool onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue);
    virtual bool onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode);
    virtual Control::Handler onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName);
    
public:
    CC_SYNTHESIZE_PASS_BY_REF(bool, m_bActive, Active);     // active status
    CC_SYNTHESIZE_PASS_BY_REF(int, m_iStarNum, StarNum);    // star grade
    
    CC_SYNTHESIZE(TollgateIconNodeDelegate*, m_iconDelegate, TollgateIconDelegate);
    
public:
    void updateIconSprite();
    
private:
    void onIconClicked(Ref* pSender, Control::EventType pControlEvent);
    
private:
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(int, m_iTollgateId, TollgateId);         // ccbi property
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(string, m_iconSptFrame, IconSptFrame);   // ccbi property
    
    Sprite* m_pIconSprite;      // ccbi ref
    
};

CLASS_NODE_LOADER2(TollgateIconNode);

class TollgateIconNodeDelegate
{
public:
    virtual void tollgateIconNodeClicked(TollgateIconNode* iconNode) {};
};

#endif /* defined(__MyGame01__TollgateIconNode__) */

