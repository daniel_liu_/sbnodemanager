//
//  TollgateIconNode.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 9/5/14.
//
//

#include "TollgateIconNode.h"

TollgateIconNode::TollgateIconNode()
{
    m_bActive = false;
    m_iStarNum = 0;
    
    m_iTollgateId = -1;
    m_pIconSprite = nullptr;
}

TollgateIconNode::~TollgateIconNode()
{
    CC_SAFE_RELEASE(m_pIconSprite);
}

void TollgateIconNode::onEnterTransitionDidFinish()
{
    updateIconSprite();
    
    Node::onEnterTransitionDidFinish();
}

bool TollgateIconNode::onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue)
{
    if (pTarget == this) {
        if (0 == strcmp(pMemberVariableName, "tollgateId")) { m_iTollgateId = pValue.asInt(); }
        if (0 == strcmp(pMemberVariableName, "iconSpriteframe")) { m_iconSptFrame = pValue.asString(); }
    }
    return true;
}

bool TollgateIconNode::onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode)
{
    SB_MEMBERVARIABLEASSIGNER_GLUE(this, "m_pIconSprite", Sprite*, m_pIconSprite);
    
    return true;
}

Control::Handler TollgateIconNode::onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName)
{
    CCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, "onIconClicked", TollgateIconNode::onIconClicked);
    
    return nullptr;
}

void TollgateIconNode::updateIconSprite()
{
    string sptframeName = StringUtils::format(m_iconSptFrame.c_str(), m_iStarNum);
    m_pIconSprite->setSpriteFrame(sptframeName.c_str());
    
    if (m_bActive == false) {
        m_pIconSprite->setColor(Color3B::GRAY);
    }else {
        m_pIconSprite->setColor(Color3B::WHITE);
    }
}

void TollgateIconNode::onIconClicked(Ref* pSender, Control::EventType pControlEvent)
{
    if (m_iconDelegate) {
        m_iconDelegate->tollgateIconNodeClicked(this);
    }
}

