//
//  ReqObjTest.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 1/9/15.
//
//

#include "ReqObjTest.h"

ReqObjTest::ReqObjTest()
{
    SBLOG_REGIST();
}

ReqObjTest::~ReqObjTest()
{
    SBLOG("~ ReqObjTest Dealloc ~");
}

void ReqObjTest::runTest()
{
    SBLOG("Run Test~~~");
    
    ReqObjManager* reqObjMgr = ReqObjManager::getInstance();
    
    cocos2d::ValueMap loginMap;
    loginMap["momo_id"] = "dVVvSE9BNzE3TVlkbUxCbGxFZFdudz09";
    loginMap["login_os"] = CC_TARGET_PLATFORM;
    loginMap["system_version"] = "9.0.1";
    loginMap["vtoken"] = "85000f9e1efa1c0b6834ceffbccf733254b3679e";
    loginMap["phone_type"] = "iPhone 7 Plus";
    loginMap["net_type"] = "Wifi";
    cocos2d::Value value_ = cocos2d::Value(loginMap);
    
    //  login request
    auto reqObj = RequestObject::create("http://120.132.53.62:8801/game/login", &value_);
    reqObj->setReqObjCallback(CC_CALLBACK_2(ReqObjTest::responseTesting, this));
    
    HttpRequest* request = reqObj->getRequest();
    setHeader(request, 1);
    reqObjMgr->addRequestObject(reqObj);
    
    //  invite message request
    auto reqObj2 = RequestObject::create("http://121.132.53.62:8801/game/getInvitedMessages", nullptr);
    reqObj2->setReqObjCallback(CC_CALLBACK_2(ReqObjTest::responseTesting2, this));
    reqObjMgr->addRequestObject(reqObj2);
    
    //  invite message request2
    auto reqObj3 = RequestObject::create("http://120.132.53.62:8801/game/getInvitedMessages", nullptr);
    reqObj3->setReqObjCallback(CC_CALLBACK_2(ReqObjTest::responseTesting3, this));
    
    HttpRequest* request3 = reqObj3->getRequest();
    setHeader(request3, 2);
    reqObjMgr->addRequestObject(reqObj3);
    
    //  to manager
    reqObjMgr->startRequestQueue();
}

void ReqObjTest::setHeader(HttpRequest *request, int msgid)
{
    std::vector<std::string> headers = request->getHeaders();
    headers.push_back(StringUtils::format("msgid:%d", msgid));
    request->setHeaders(headers);
}

void ReqObjTest::responseTesting(bool success, Value value)
{
    SBLOG("response testing: %d & desc: %s", success, value.getDescription().c_str());
}

void ReqObjTest::responseTesting2(bool success, Value value)
{
    SBLOG("response testing2: %d & desc: %s", success, value.getDescription().c_str());
}

void ReqObjTest::responseTesting3(bool success, Value value)
{
    SBLOG("response testing3: %d & desc: %s", success, value.getDescription().c_str());
}

