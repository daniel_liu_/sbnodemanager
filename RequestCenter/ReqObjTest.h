//
//  ReqObjTest.h
//  MyGame01
//
//  Created by LiuHuanMing on 1/9/15.
//
//

#ifndef __MyGame01__ReqObjTest__
#define __MyGame01__ReqObjTest__

#include "RequestObject.h"

#include "ReqObjManager.h"

class ReqObjTest : public Ref
{
private:
    ReqObjTest();
    ~ReqObjTest();
    
    virtual bool init() { return true; };
    
public:
    CREATE_FUNC(ReqObjTest);
    
public:
    void runTest();
    void setHeader(HttpRequest *request, int msgid);
    
    void responseTesting(bool success, Value value);
    void responseTesting2(bool success, Value value);
    void responseTesting3(bool success, Value value);
    
};

#endif /* defined(__MyGame01__ReqObjTest__) */
