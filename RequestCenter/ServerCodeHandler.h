//
//  ServerCodeHandler.h
//  MyGame01
//
//  Created by LiuHuanMing on 1/15/15.
//
//

#ifndef __MyGame01__ServerCodeHandler__
#define __MyGame01__ServerCodeHandler__

#include "cocos2d.h"

//  ServerCode Enmu
typedef enum E_SEVER_CODE
{
    CODE_CLOSETEST_PLAYERLIMIT = -14,//封测期间登陆人数已超上限
    CODE_SEVERERRO = -1,
    CODE_SUCCESS = 1,
    CODE_LOGINTIMEOUT = 1001,//登陆超时或者还未登陆。需要客户端重新发起登陆请求。
    CODE_SENDHEARTINCD = 1002,//送心CD中。
    CODE_HEARTNOTENOUGHT = 1003,//心不足。
    CODE_ITEMINTOLLERROR = 1004,//该关卡无法使用此道具({0}为道具ID)。
    CODE_BOXOPENED = 1005,//该宝箱已经开过了。
    CODE_STARNOTENOUGHT = 1006,//星星不足，无法开启宝箱。
    CODE_CANNOTACCEPTHEART = 1007,//心已经上限，无法继续收取心。
    CODE_ITEMNUMBERNOTENOUGHT = 1008,//道具数量不足，无法使用。
    CODE_CANNOTSENDHEARTTOSELF = 1009,//不能给自己送心。
    CODE_FRIENDCLOSEACCEPTHEART = 1010,//好友不接受送心。
    CODE_PRODUCTNOTEXISIT = 1011,//商品不存在。
    CODE_FRIENDHASINVITED = 1012,//好友已经被邀请过了，无法再次被邀请。
    CODE_MAILNOTEXISIT = 1013,//邮件不存在
    CODE_DIAMOND = 1014,//钻石不足
    CODE_TOLLGATENOTUNLOCK = 1015, //关卡未解锁
    CODE_GOLDNOTENOUGH = 1016,
    CODE_BUILDINGNOTUNLOCK = 1017,
    CODE_BUILDINGALREADYUNLOCKBYFRIEND = 1018,//建组已由好友帮忙解锁
    CODE_BUFFNOTOPEN = 1019,//buff未激活
    CODE_BUFFTIMENOTREACH = 1020,//buff时间未到，不能领取收获
    CODE_ITEMNOTEXISIT = 1021,//道具不存在
}SEVER_CODE;

class ServerCodeHandler : public cocos2d::Ref
{
public:
    bool handleServerCode(int code);
    
};

#endif /* defined(__MyGame01__ServerCodeHandler__) */

