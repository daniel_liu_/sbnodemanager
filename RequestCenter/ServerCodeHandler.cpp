//
//  ServerCodeHandler.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 1/15/15.
//
//

#include "ServerCodeHandler.h"

//  应该关联公共的UI与数据组件类
bool ServerCodeHandler::handleServerCode(int code)
{
    switch (code) {
        case CODE_SUCCESS: {
            SBLOG("返回成功");
            return true;
        }
        case CODE_CLOSETEST_PLAYERLIMIT: {
            SBLOG("无封测资格，敬请期待公测!");
            return false;
        }
        case CODE_SEVERERRO: {
            SBLOG("系统错误 -1");
            return false;
        }
        default: {
            SBLOG("未捕获的异常 code=%d", code);
            return false;
        }
    }
}

