//
//  StringUtil.h
//  MyGame01
//
//  Created by LiuHuanMing on 1/12/15.
//
//

#ifndef MyGame01_StringUtil_h
#define MyGame01_StringUtil_h

#include "external/unzip/unzip.h"
#include <zlib.h>

#define SEGMENT_SIZE 1024 * 16  //接收的缓存，单位是字节

char* ungZip(string source)
{
    unsigned char xxx[SEGMENT_SIZE];
    int len = source.size()-1;
    for (int i = 0;i< (int)source.size()-1;i++)
    {
        xxx[i] = source[i];
        //CCLOG("%x ",xxx[i]);
    }
    int err;
    z_stream d_stream;
    Byte compr[SEGMENT_SIZE]={0}, uncompr[SEGMENT_SIZE*4]={0};
    memcpy(compr,(Byte*)xxx,len);
    //free(xxx);
    uLong comprLen, uncomprLen;
    comprLen = sizeof(compr) / sizeof(compr[0]);
    uncomprLen = 4*comprLen;
    strcpy((char*)uncompr, "garbage");
    d_stream.zalloc = (alloc_func)0;
    d_stream.zfree = (free_func)0;
    d_stream.opaque = (voidpf)0;
    d_stream.next_in = compr;
    d_stream.avail_in = 0;
    d_stream.next_out = uncompr;
    err = inflateInit2(&d_stream,47);
    if(err!=Z_OK)
    {
        printf("inflateInit2 error:%d",err);
        return NULL;
    }
    while (d_stream.total_out < uncomprLen && d_stream.total_in < comprLen) {
        d_stream.avail_in = d_stream.avail_out = 1;
        err = inflate(&d_stream,Z_NO_FLUSH);
        if(err == Z_STREAM_END) break;
        if(err!=Z_OK)
        {
            printf("inflate error:%d",err);
            return NULL;
        }
    }
    err = inflateEnd(&d_stream);
    if(err!=Z_OK)
    {
        printf("inflateEnd error:%d",err);
        return NULL;
    }
    char* b = new char[d_stream.total_out+1];
    memset(b,0,d_stream.total_out+1);
    memcpy(b,(char*)uncompr,d_stream.total_out);
    return b;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

std::vector<std::string> Split(std::string str,std::string pattern)
{
    std::string::size_type pos;
    std::vector<std::string> result;
    
    str+=pattern;       //扩展字符串以方便操作
    int size=str.size();
    
    for(int i=0; i<size; i++)
    {
        pos=str.find(pattern,i);
        if(pos<size)
        {
            std::string s=str.substr(i,pos-i);
            result.push_back(s);
            i=pos+pattern.size()-1;
        }
    }
    return result;
}

string charVectorToString(vector<char>* pVectorBuff)
{
    string str;
    for (int i = 0; i < pVectorBuff->size(); ++i) {
        str += (*pVectorBuff)[i];
    }
    str += '\0';
    return str;
}

string getStringByKey(std::vector<char>* datas_, string key_)
{
    string headerStr = charVectorToString(datas_);
    CCLOG("get string by key: %s, key_: %s", headerStr.c_str(), key_.c_str());
    
    vector<string> compons = Split(headerStr, "\r\n");
    for (int i  = 0; i < compons.size(); i++) {
        if (compons[i].find(key_) != std::string::npos) {
            CCLOG("string by key: %s", compons[i].c_str());
            vector<string> pair = Split(compons[i], ": ");
            string _key_ = pair[0];
            string _value_ = pair[1];
            CCLOG("key & value: |%s|, |%s|", _key_.c_str(), _value_.c_str());
            
            return _value_;
        }
    }
    
    return "";
}

#endif
