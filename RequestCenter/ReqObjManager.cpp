//
//  ReqObjManager.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 1/15/15.
//
//

#include "ReqObjManager.h"

static ReqObjManager *_sharedReqObjectManager = nullptr;

ReqObjManager* ReqObjManager::getInstance()
{
    if (!_sharedReqObjectManager)
    {
        _sharedReqObjectManager = new ReqObjManager();
        _sharedReqObjectManager->init();
    }
    
    return _sharedReqObjectManager;
}

void ReqObjManager::destroyInstance()
{
    CC_SAFE_RELEASE_NULL(_sharedReqObjectManager);
}

bool ReqObjManager::init()
{
    _requestObjects.clear();
    
    m_bIsRunning = false;
    m_iRetryTimes = 0;
    m_iReqIndex = 0;
    
    return true;
}

void ReqObjManager::managerHttpResponseCallback(HttpClient *sender, HttpResponse *response)
{
    RequestObject* reqObj = _requestObjects.at(m_iReqIndex);
    CCASSERT(reqObj, "Request Object can not be nil!! [ReqObjManager]");
    
    bool success = response->isSucceed();
    if (success)
    {
        //  交给自己去处理
        reqObj->httpResponseCallback(sender, response);
        m_bIsRunning = false;
        
        //  继续下一个
        m_iReqIndex++;
        startRequestQueue();
    }
    else
    {
        SBLOG("request [%s] is fail, retry count? %d", reqObj->getIdentity().c_str(), reqObj->getRetryCount());
        
        int count_ = reqObj->getRetryCount();
        if (count_ < 0)
        {
            //  TODO:: 提示给用户
        }
        else
        {
            //  重试x次
            m_iRetryTimes++;
            SBLOG("retry times: %d", m_iRetryTimes);
            if (m_iRetryTimes <= count_) {
                reqObj->startRequest();
            }
            else {
                //  不重试(交给自己去处理)
                reqObj->httpResponseCallback(sender, response);
                m_bIsRunning = false;
                
                //  继续下一个
                m_iReqIndex++;
                startRequestQueue();
            }
        }
    }
}

void ReqObjManager::addRequestObject(RequestObject* req_)
{
    //  check contain
    if (containRequestObject(req_)) {
        return;
    }
    req_->retain();
    _requestObjects.push_back(req_);
}

void ReqObjManager::removeRequestObject(RequestObject* req_)
{
    std::vector<RequestObject*>::iterator iter;
    for (iter = _requestObjects.begin(); iter != _requestObjects.end(); iter++) {
        if ((*iter) == req_) {
            _requestObjects.erase(iter);
            (*iter)->release();
        }
    }
}

void ReqObjManager::removeAllRequest()
{
    for (int i = 0; i < _requestObjects.size(); i++) {
        RequestObject* req_ = dynamic_cast<RequestObject*>(_requestObjects.at(i));
        req_->release();
    }
    _requestObjects.clear();
}

bool ReqObjManager::containRequestObject(RequestObject* req_)
{
    for (int i = 0; i < _requestObjects.size(); i++)
    {
        RequestObject* req = dynamic_cast<RequestObject*>(_requestObjects.at(i));
        if (req == req_) {
            return true;
        }
    }
    return false;
}

std::vector<RequestObject*> ReqObjManager::findSimilarRequests(RequestObject* req_)
{
    std::vector<RequestObject*> vec_;
    for (int i = 0; i < _requestObjects.size(); i++)
    {
        RequestObject* req = dynamic_cast<RequestObject*>(_requestObjects.at(i));
        if (req->getIdentity().compare(req_->getIdentity()) == 0) {
            vec_.push_back(req);
        }
    }
    return vec_;
}

void ReqObjManager::dumpInfo()
{
    //  iterator
    for (int i = 0; i < _requestObjects.size(); i++)
    {
        RequestObject* req_ = dynamic_cast<RequestObject*>(_requestObjects.at(i));
        SBLOG("req_: %s - %p", req_->getIdentity().c_str(), req_);
    }
}

void ReqObjManager::startRequestQueue()
{
    if (getIsRunning()) {
        SBLOG("Request Queue 正在进行中");
        return;
    }
    
    bool isInList = (_requestObjects.size() > m_iReqIndex);
    if (!isInList)
    {
        m_bIsRunning = false;
        m_iRetryTimes = 0;
        
        m_iReqIndex = 0;
        removeAllRequest();
        
        SBLOG("ReqObjManager队列已经执行完成");
    }
    else
    {
        m_bIsRunning = true;
        m_iRetryTimes = 0;
        
        RequestObject* req_ = dynamic_cast<RequestObject*>(_requestObjects.at(m_iReqIndex));
        CCASSERT(req_, "RequestObject can not be nil!!! [startRequestQueue]");
        
        req_->getRequest()->setResponseCallback(CC_CALLBACK_2(ReqObjManager::managerHttpResponseCallback, this));
        req_->startRequest();
        
        SBLOG("找到当前 RequestObject: %s, %p", req_->getIdentity().c_str(), req_);
    }
}

