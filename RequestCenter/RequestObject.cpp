//
//  RequestObject.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 1/8/15.
//
//

#include "RequestObject.h"

#include "GPJsonHelper.h"
#include "StringUtil.h"
#include "Rijndael.h"

RequestObject::RequestObject()
{
    SBLOG_REGIST();
    
    m_pRequest = nullptr;
    m_pSCodeHandler = nullptr;
    
    m_pCallback = nullptr;
    
    m_iRetryCount = 1;
    m_bDistinct = false;
    m_bOptional = true;
}

RequestObject::~RequestObject()
{
    CC_SAFE_RELEASE(m_pRequest);
    CC_SAFE_RELEASE(m_pSCodeHandler);
    
    SBLOG("~ RequestObject Dealloc ~");
}

void RequestObject::initRequest(const char *url_, Value *root)
{
    CCASSERT(m_pRequest == nullptr, "Request is already inited [RequestObject]");
    
    //  init request
    HttpRequest* request_ = new HttpRequest();
    
    request_->setUrl(url_);
    request_->setRequestType(HttpRequest::Type::POST);
    request_->setResponseCallback(CC_CALLBACK_2(RequestObject::httpResponseCallback, this));
    
    //  identity use url_ as default
    setIdentity(url_);
    
    //  datas
    if (root && !root->isNull())
    {
        //  TODO::如果加密的算法或方式比较多，则尽量使用装饰者模式来对RequestObject进行加工处理
        string content_ = root->getDescription();
        SBLOG("contents: %s", content_.c_str());
        const char* str_ = encryt(content_, "1234567890123456").c_str();
        request_->setRequestData(str_, strlen(str_));
    }
    
    //  TODO:: can change this at runtime
    ServerCodeHandler* handler = new ServerCodeHandler();
    handler->autorelease();
    setSCodeHandler(handler);
    
    //  save ref
    m_pRequest = request_;
}

RequestObject* RequestObject::create(const char *url_, Value *root_)
{
    RequestObject* pRet = new RequestObject();
    if (pRet) {
        pRet->initRequest(url_, root_);
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

void RequestObject::startRequest()
{
    CCASSERT(getRequest(), "Call initRequest at first [RequetsObject]");
    
    HttpClient::getInstance()->send(getRequest());
}

void RequestObject::httpResponseCallback(HttpClient *sender, HttpResponse *response)
{
    if (response->isSucceed())
    {
        //  success
        bool flag = false;
        
        //  unGzip
        string needUnGzip = getStringByKey(response->getResponseHeader(), "unGzip");
        bool unGzip = (needUnGzip.compare("true") == 0);
        SBLOG("need unGzip: %s & unGzip: %d", needUnGzip.c_str(), unGzip);
        
        string responseStr = charVectorToString(response->getResponseData());
        if (unGzip) { responseStr = ungZip(responseStr); }
        
        //  decrypt
        responseStr = decryt(responseStr, "1234567890123456");
        
        //  result
        SBLOG("[RequestObject] SUCCESS:\n %s", responseStr.c_str());
        
        //  可利用策略模式对返回的内容进行封装处理
        //  TODO:: 这里先测试为json返回，其实也里也应该更灵活的处理
        ValueMap vMap = GPJsonHelper::parseJson2Value(responseStr).asValueMap();
        if (getSCodeHandler()) {
            flag = getSCodeHandler()->handleServerCode(vMap["code"].asInt());
        }
        
        //  交给回调处理
        if (getReqObjCallback() != nullptr) {
            getReqObjCallback()(flag, Value(vMap));
        }
    }
    else
    {
        SBLOG("[RequestObject] FAIL:\n %s", response->getErrorBuffer());
        
        //  交给回调处理
        if (getReqObjCallback() != nullptr) {
            getReqObjCallback()(false, Value());
        }
    }
}

