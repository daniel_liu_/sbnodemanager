//
//  GPJsonHelper.h
//  growthpush-cocos2dx
//
//  Created by TSURUDA Ryo on 2013/12/11.
//  Copyright (c) 2013年 TSURUDA Ryo. All rights reserved.
//

#ifndef __GROWTHPUSHPLUGIN_GPJSONHELPER_H__
#define __GROWTHPUSHPLUGIN_GPJSONHELPER_H__

#include "cocos2d.h"

#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"

class GPJsonHelper
{
public:
    
    /**
     * Parse JSON string to Value.
     *
     * @param json  JSON string.
     * @return parsed Value value.
     */
    static cocos2d::Value parseJson2Value(const std::string &json) {
        rapidjson::Document d;
        
        if (d.Parse<0>(json.c_str()).HasParseError()) {
            CCLOGERROR("failed to parse json:%s", d.GetParseError());
            return cocos2d::Value::Null;
        }
        return convertJson2Value(d);
    }
    
public:
    
    static cocos2d::Value convertJson2Value(const rapidjson::Value &v) {
        
        if (v.IsObject()) {
            return convertJson2Map(v);
        }
        if (v.IsArray()) {
            return convertJson2Vector(v);
        }
        if (v.IsString()) {
            return convertJson2String(v);
        }
        if (v.IsInt()) {
            return convertJson2Int(v);
        }
        if (v.IsNumber()) {
            return convertJson2Double(v);
        }
        if (v.IsBool()) {
            return convertJson2Bool(v);
        }
        if (v.IsNull()) {
            return convertJson2Null(v);
        }
        
        CCLOGERROR("failed to convert: Unknown value type");
        return cocos2d::Value::Null;
        
    }
    
private:
    
    static cocos2d::Value convertJson2Map(const rapidjson::Value &v) {
        
        cocos2d::ValueMap dictionary;
        
        for (rapidjson::Value::ConstMemberIterator it = v.MemberonBegin(); it != v.MemberonEnd(); ++it) {
            cocos2d::Value value = convertJson2Value(it->value);
            dictionary.insert(std::make_pair(it->name.GetString(), value));
        }
        return cocos2d::Value(dictionary);
        
    }
    
    static cocos2d::Value convertJson2Vector(const rapidjson::Value &v) {
        
        cocos2d::ValueVector array;
        
        for (rapidjson::Value::ConstValueIterator it = v.onBegin(); it != v.onEnd(); ++it) {
            cocos2d::Value value = convertJson2Value(*it);
            array.push_back(value);
        }
        return cocos2d::Value(array);
        
    }
    
    static cocos2d::Value convertJson2String(const rapidjson::Value &v) {
        
        std::string s = v.GetString();
        
        return cocos2d::Value(s);
        
    }
    
    static cocos2d::Value convertJson2Int(const rapidjson::Value &v) {
        
        int d = v.GetInt();
        
        return cocos2d::Value(d);
        
    }
    
    static cocos2d::Value convertJson2Double(const rapidjson::Value &v) {
        
        double d = v.GetDouble();
        
        return cocos2d::Value(d);
        
    }
    
    static cocos2d::Value convertJson2Bool(const rapidjson::Value &v) {
        
        bool b = v.GetBool();
        
        return cocos2d::Value(b);
        
    }
    
    static cocos2d::Value convertJson2Null(const rapidjson::Value &v) {
        
        CC_UNUSED_PARAM(v);      // ignore value
        return cocos2d::Value::Null;
        
    }
    
private:
    
    static rapidjson::Value* jsonValueFromValue(cocos2d::Value v, rapidjson::Document *document)
    {
        auto t = v.getType();
        
        if (t == cocos2d::Value::Type::BOOLEAN) {
            rapidjson::Value *value = new rapidjson::Value(v.asBool());
            return value;
        }
        
        if (t == cocos2d::Value::Type::FLOAT) {
            rapidjson::Value *value = new rapidjson::Value(v.asFloat());
            return value;
        }
        
        if (t == cocos2d::Value::Type::DOUBLE) {
            rapidjson::Value *value = new rapidjson::Value(v.asDouble());
            return value;
        }
        
        if (t == cocos2d::Value::Type::INTEGER) {
            rapidjson::Value *value = new rapidjson::Value(v.asInt());
            return value;
        }
        
        if (t == cocos2d::Value::Type::STRING) {
            rapidjson::Value *value = new rapidjson::Value(v.asString().c_str());
            return value;
        }
        
        if (t == cocos2d::Value::Type::VECTOR) {
            rapidjson::Value *value = GPJsonHelper::jsonValueFromValueVector(v.asValueVector(), document);
            return value;
        }
        
        if (t == cocos2d::Value::Type::MAP) {
            rapidjson::Value *value = GPJsonHelper::jsonValueFromValueMap(v.asValueMap(), document);
            return value;
        }
        
        //  default null
        return new rapidjson::Value(rapidjson::kNullType);
    }
    
    static rapidjson::Value* jsonValueFromValueMap(cocos2d::ValueMap map, rapidjson::Document *document)
    {
        rapidjson::Value* value = new rapidjson::Value(rapidjson::kObjectType);
        
        bool isAllocatorDocument = false;
        if (!document) {
            document = new rapidjson::Document();
            isAllocatorDocument = true;
        }
        
        for (auto iter = map.cbegin(); iter != map.cend(); ++iter) {
            const char * key_ = iter->first.c_str();
            rapidjson::Value *value_ = GPJsonHelper::jsonValueFromValue(iter->second, document);
            value->AddMember(key_, value_, document->GetAllocator());
            delete value_;
        }
        
        if (isAllocatorDocument) {
            delete document;
        }
        
        return value;
    }
    
    static rapidjson::Value* jsonValueFromValueVector(cocos2d::ValueVector vector, rapidjson::Document *document)
    {
        rapidjson::Value* value = new rapidjson::Value(rapidjson::kArrayType);
        
        bool isAllocatorDocument = false;
        if (!document) {
            document = new rapidjson::Document();
            isAllocatorDocument = true;
        }
        
        for (auto& v_ : vector)
        {
            rapidjson::Value *value_ = GPJsonHelper::jsonValueFromValue(v_, document);
            value->PushBack(value_, document->GetAllocator());
            delete value_;
        }
        
        if (isAllocatorDocument) {
            delete document;
        }
        
        return value;
    }
    
public:
    
    //  fail
    static void testCase1()
    {
        cocos2d::ValueMap map_;
        map_["name"] = "daniel";
        map_["age"] = 28;
        map_["height"] = 1.67;
        
        cocos2d::ValueVector vect_;
        vect_.push_back(cocos2d::Value("value1"));
        vect_.push_back(cocos2d::Value("value1"));
        vect_.push_back(cocos2d::Value(333));
        map_["arr"] = vect_;
        
        cocos2d::Value value_(map_);
        CCLOG("datas: %s", value_.getDescription().c_str());
        
        rapidjson::Value* rValue = GPJsonHelper::jsonValueFromValueMap(map_, nullptr);
        
        //  print format
        rapidjson::StringBuffer buffer;
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        rValue->Accept(writer);
        
        const char *str_ = buffer.GetString();
        CCLOG("str_: %s", str_);
    }
    
    //  success
    static void testCase2()
    {
        //  json
        rapidjson::Document document;
        document.SetObject();
        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();
        
        rapidjson::Value object(rapidjson::kObjectType);
        object.AddMember("int", 1, allocator);
        object.AddMember("double", 1.0, allocator);
        object.AddMember("bool", true, allocator);
        object.AddMember("hello", "你好", allocator);
        
        rapidjson::Value array(rapidjson::kArrayType);
        array.PushBack(object, allocator);
        
        document.AddMember("json", "json string", allocator);
        document.AddMember("array", array, allocator);
        
        //  json
        cocos2d::Value value_ = GPJsonHelper::convertJson2Value(document);
        string desc = value_.getDescription();
        CCLOG("desc: %s", desc.c_str());
    }
    
};

#endif  // __GROWTHPUSHPLUGIN_GPJSONHELPER_H__

