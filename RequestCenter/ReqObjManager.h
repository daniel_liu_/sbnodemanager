//
//  ReqObjManager.h
//  MyGame01
//
//  Created by LiuHuanMing on 1/15/15.
//
//

#ifndef __MyGame01__ReqObjManager__
#define __MyGame01__ReqObjManager__

#include "base/CCRef.h"
#include "base/CCValue.h"
#include "base/CCMap.h"

#include <set>
#include <string>

#include "RequestObject.h"

class ReqObjManager : public cocos2d::Ref
{
public:
    static ReqObjManager* getInstance();
    static void destroyInstance();
    
protected:
    ReqObjManager(){
        SBLOG_REGIST();
    }
    
public:
    virtual ~ReqObjManager(){};
    bool init();
    
private:
    void managerHttpResponseCallback(HttpClient *sender, HttpResponse *response);
    
public:
    void addRequestObject(RequestObject* req_);
    void removeRequestObject(RequestObject* req_);
    void removeAllRequest();
    bool containRequestObject(RequestObject* req_);
    std::vector<RequestObject*> findSimilarRequests(RequestObject* req_);
    
    void dumpInfo();
    
    void startRequestQueue();
    
protected:
    std::vector<RequestObject*> _requestObjects;
    
    //  有请求在运行中
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(bool, m_bIsRunning, IsRunning);
    //  重试过的次数
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(int, m_iRetryTimes, RetryTimes);
    //  当前index
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(int, m_iReqIndex, ReqIndex);
    
};

#endif /* defined(__MyGame01__ReqObjManager__) */
