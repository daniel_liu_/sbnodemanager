//
//  RequestObject.h
//  MyGame01
//
//  Created by LiuHuanMing on 1/8/15.
//
//

#ifndef __MyGame01__RequestObject__
#define __MyGame01__RequestObject__

#include "cocos2d.h"
#include "cocos-ext.h"

#include "network/HttpClient.h"
#include "ServerCodeHandler.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace network;
using namespace std;

typedef std::function<void(bool success, Value value)> ccRequestObjectCallback;

class RequestObject : public Ref
{
private:
    RequestObject();
    ~RequestObject();
    
    void initRequest(const char *url_, Value *root_);
    
public:
    static RequestObject* create(const char *url_, Value *root_);
    
    void startRequest();
    void httpResponseCallback(HttpClient *sender, HttpResponse *response);
    
private:
    // request
    CC_SYNTHESIZE_READONLY(HttpRequest*, m_pRequest, Request);
    
    //  handlers
    CC_SYNTHESIZE_RETAIN(ServerCodeHandler*, m_pSCodeHandler, SCodeHandler);
    
    //  callback
    CC_SYNTHESIZE_PASS_BY_REF(ccRequestObjectCallback, m_pCallback, ReqObjCallback);
    
    //  identity - 用来区分是否为相同功能的请求
    CC_SYNTHESIZE_PASS_BY_REF(string, m_sIdentity, Identity);
    
    //  retry count - 自动重试次数(<0为提示，0为不重试，x为次数)
    CC_SYNTHESIZE_PASS_BY_REF(int, m_iRetryCount, RetryCount);
    
    //  distinct - 是否独占，用于队列中，如果标记为true，则其它相同的不法再添加
    CC_SYNTHESIZE_PASS_BY_REF(bool, m_bDistinct, Distinct);
    
    //  optional - 非必要(队列中可重复添加，如果为true，则队列中执行过相同请求时，同类都调用回调，并将同类全移除掉)
    CC_SYNTHESIZE_PASS_BY_REF(bool, m_bOptional, Optional);
    
};

#endif /* defined(__MyGame01__RequestObject__) */

