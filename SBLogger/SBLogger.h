//
//  SBLogger.h
//  MyGame01
//
//  Created by LiuHuanMing on 1/19/15.
//
//

#ifndef __MyGame01__SBLogger__
#define __MyGame01__SBLogger__

#include "base/CCRef.h"
#include "base/CCConsole.h"
#include "base/CCDirector.h"

#define SBLOG_REGIST()          SBLogger::getInstance()->registLogger(typeid(this).name())
#define SBLOG_REGIST2(name)     SBLogger::getInstance()->registLogger(name)

#define SBLOG_DISABLE()         SBLogger::getInstance()->enableLogger(typeid(this).name(), false)
#define SBLOG_DISABLE2(name)    SBLogger::getInstance()->enableLogger(name, false)

#define SBLOG(format, ...)          SBLogger::getInstance()->log(typeid(this).name(), format, ##__VA_ARGS__)
#define SBLOG2(name, format, ...)   SBLogger::getInstance()->log(name, format, ##__VA_ARGS__)

#ifdef CCLOG//(format, ...)
#undef CCLOG//(format, ...)
#define CCLOG(format, ...)  SBLogger::getInstance()->log("TEMP", format, ##__VA_ARGS__)
#endif

class SBLogger : public cocos2d::Ref
{
public:
    static SBLogger* getInstance();
    static void destroyInstance();
    
protected:
    SBLogger(){}
    
public:
    virtual ~SBLogger(){};
    bool init();
    
public:
    void enableLogger(const char* loggerName, bool isEnable);
    void enableAllLoggers(bool isEnable);
    
    void registLogger(const char* loggerName);
    
    bool isLoggerEnabled(const char* loggerName);
    
    void log(const char* loggerName, const char * format, ...);
    
protected:
    std::map<std::string, bool> _loggers;
    
};

#endif /* defined(__MyGame01__SBLogger__) */

