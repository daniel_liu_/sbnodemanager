//
//  SBLogger.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 1/19/15.
//
//

#include "SBLogger.h"

static SBLogger *_sharedSBLogger = nullptr;

SBLogger* SBLogger::getInstance()
{
    if (!_sharedSBLogger)
    {
        _sharedSBLogger = new SBLogger();
        _sharedSBLogger->init();
    }
    
    return _sharedSBLogger;
}

void SBLogger::destroyInstance()
{
    CC_SAFE_RELEASE_NULL(_sharedSBLogger);
}

bool SBLogger::init()
{
    _loggers.clear();
    
    return true;
}

void SBLogger::enableLogger(const char* loggerName, bool isEnable)
{
    for (auto iter = _loggers.begin(); iter != _loggers.end(); ++iter)
    {
        std::string name_ = iter->first;
        if (name_.compare(loggerName) == 0) {
            iter->second = isEnable;
        }
    }
}

void SBLogger::enableAllLoggers(bool isEnable)
{
    for (auto iter = _loggers.begin(); iter != _loggers.end(); ++iter)
    {
        iter->second = isEnable;
    }
}

void SBLogger::registLogger(const char* loggerName)
{
    _loggers.insert(std::make_pair(loggerName, true));
}

bool SBLogger::isLoggerEnabled(const char* loggerName)
{
    
#if !defined(COCOS2D_DEBUG) || COCOS2D_DEBUG == 0
    return false;
#endif
    
    bool enabled = false;
    for (auto iter = _loggers.begin(); iter != _loggers.end(); ++iter)
    {
        std::string name_ = iter->first;
        if (name_.compare(loggerName) == 0) {
            enabled = iter->second;
        }
    }
    return enabled;
}

//  copy from CCConsole.cpp
static void _log(const char *format, va_list args)
{
    char buf[cocos2d::MAX_LOG_LENGTH];
    
    vsnprintf(buf, cocos2d::MAX_LOG_LENGTH-3, format, args);
    strcat(buf, "\n");
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    __android_log_print(ANDROID_LOG_DEBUG, "cocos2d-x debug info",  "%s", buf);
    
#elif CC_TARGET_PLATFORM ==  CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_WINRT || CC_TARGET_PLATFORM == CC_PLATFORM_WP8
    WCHAR wszBuf[MAX_LOG_LENGTH] = {0};
    MultiByteToWideChar(CP_UTF8, 0, buf, -1, wszBuf, sizeof(wszBuf));
    OutputDebugStringW(wszBuf);
    WideCharToMultiByte(CP_ACP, 0, wszBuf, -1, buf, sizeof(buf), nullptr, FALSE);
    printf("%s", buf);
    fflush(stdout);
#else
    // Linux, Mac, iOS, etc
    fprintf(stdout, "cocos2d: %s", buf);
    fflush(stdout);
#endif
    
#if (CC_TARGET_PLATFORM != CC_PLATFORM_WINRT)
    cocos2d::Director::getInstance()->getConsole()->log(buf);
#endif
    
}

void SBLogger::log(const char* loggerName, const char * format, ...)
{
    if (isLoggerEnabled(loggerName))
    {
        va_list args;
        va_start(args, format);
        _log(format, args);
        va_end(args);
    }
}

