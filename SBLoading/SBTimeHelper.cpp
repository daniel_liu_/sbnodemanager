//
//  SBTimeHelper.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 8/30/14.
//
//

#include "SBTimeHelper.h"
#include "cocos2d.h"

SBTimeHelper* SBTimeHelper::m_pTimeHelper = NULL;

SBTimeHelper* SBTimeHelper::getTimeHelper()
{
    if (NULL == m_pTimeHelper)
    {
        m_pTimeHelper = new SBTimeHelper();
        return m_pTimeHelper;
    }
    return m_pTimeHelper;
}

SBTimeHelper::SBTimeHelper()
{
    m_iLastTimeStamp = 0;
}

tm* SBTimeHelper::getLocalTime()
{
    time_t s_time;
    struct tm *p;
    
    time(&s_time);
    p = localtime(&s_time);
    
    return p;
}

long SBTimeHelper::getLocalTimeTick()
{
    time_t stime;
    struct tm *p;
    
    time(&stime);
    p = localtime(&stime);
    
    return mktime(p);
}

long SBTimeHelper::getLocalTimeMTick()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

std::string SBTimeHelper::getStringByTick(long tick)
{
    time_t t = tick;
    struct tm *p;
    p = localtime(&t);
    char s[100];
    strftime(s, sizeof(s), "%Y-%m-%d %H:%M:%S", p);
    
    return std::string(s);
}

long SBTimeHelper::recordTimeStamp()
{
    long delta = 0;
    long now = getLocalTimeMTick();
    if (m_iLastTimeStamp != 0) {
        delta = now - m_iLastTimeStamp;
        CCLOG("[SBTimeHelper] - the delta from last time stamp: %ld ms", delta);
    }
    m_iLastTimeStamp = now;
    return delta;
}

