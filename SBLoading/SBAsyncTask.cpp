//
//  SBAsyncTask.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 8/29/14.
//
//

#include "SBAsyncTask.h"
#include "SBCCBCache.h"

#pragma mark - SBAsyncTask

SBAsyncTask::SBAsyncTask()
{
    m_bStarted = false;
    
    m_pObserver = NULL;
    m_pCallBack = NULL;
}

void SBAsyncTask::start()
{
    m_bStarted = true;
}

void SBAsyncTask::setAsyncTaskObserver(Ref* pObserver, AsyncTaskCallBack pCallBack)
{
    m_pObserver = pObserver;
    m_pCallBack = pCallBack;
}

bool SBAsyncTask::isStarted()
{
    return m_bStarted;
}

#pragma mark - SBLoadTextureTask

SBLoadTextureTask::SBLoadTextureTask()
{
    m_loaded = 0;
    m_textureFiles.clear();
}

SBLoadTextureTask::~SBLoadTextureTask()
{
    CCLOG("~SBLoadTextureTask");
}

void SBLoadTextureTask::start()
{
    CCAssert(m_textureFiles.size() > 0, "size must not 0");
    
    SBAsyncTask::start();
    
    SBLoadTextureTask *selfRef = this;
    for (int i = 0; i < m_textureFiles.size(); i++)
    {
        string path = m_textureFiles.at(i);
        TextureCache *textureCache = Director::getInstance()->getTextureCache();
        auto loadingCallBack = [selfRef](Texture2D* pObject)
        {
            selfRef->m_loaded++;
            Ref* pObserver = selfRef->m_pObserver;
            AsyncTaskCallBack pCallBack = selfRef->m_pCallBack;
            if (pObserver && pCallBack) {
                (pObserver->*pCallBack)(selfRef, selfRef->isFinished(), pObject);
            }
        };
        textureCache->addImageAsync(path.c_str(), loadingCallBack);
    }
}

bool SBLoadTextureTask::isFinished()
{
    return m_loaded == m_textureFiles.size();
}

void SBLoadTextureTask::addTextrueFile(string file)
{
    m_textureFiles.push_back(file);
}

#pragma mark - SBLoadCCBITask

SBLoadCCBITask::SBLoadCCBITask()
{
    m_loaded = 0;
    m_ccbiFiles.clear();
}

SBLoadCCBITask::~SBLoadCCBITask()
{
    CCLOG("~SBLoadCCBITask()");
}

void SBLoadCCBITask::start()
{
    CCAssert(m_ccbiFiles.size() > 0, "size must bigger than 0");
    
    SBAsyncTask::start();
    
    SBLoadCCBITask *selfRef = this;
    for (int i = 0; i < m_ccbiFiles.size(); i++)
    {
        string path = m_ccbiFiles.at(i);
        auto loadingCallBack = [selfRef](Data data_)
        {
            selfRef->m_loaded++;
            Ref* pObserver = selfRef->m_pObserver;
            AsyncTaskCallBack pCallback = selfRef->m_pCallBack;
            if (pObserver && pCallback) {
                (pObserver->*pCallback)(selfRef, selfRef->isFinished(), nullptr);
            }
        };
        CCBCACHE->addCCBIAsync(path.c_str(), loadingCallBack);
    }
}

bool SBLoadCCBITask::isFinished()
{
    return m_loaded == m_ccbiFiles.size();
}

void SBLoadCCBITask::addCCBIFile(string file)
{
    m_ccbiFiles.push_back(file);
}

#pragma mark - SBAsyncTaskPool

static pthread_t            s_asyncTaskThread;

static pthread_mutex_t		s_SleepMutex;
static pthread_cond_t		s_SleepCondition;

static pthread_mutex_t      s_asyncTasksMutex;

static bool                 need_quit = false;

static vector<SBAsyncTask*>* s_pAsyncTasks = NULL;

SBAsyncTaskPool* SBAsyncTaskPool::m_pAsyncTaskPool = NULL;

static void* runAsyncTaskPool(void* data)
{
    while (true)
    {
        // create autorelease pool for iOS
        ThreadHelper::createAutoreleasePool();
        
        vector<SBAsyncTask*>* pTasks = s_pAsyncTasks;
        pthread_mutex_lock(&s_asyncTasksMutex); // get async task from queue
        if (pTasks->empty())
        {
            pthread_mutex_unlock(&s_asyncTasksMutex);
            if (need_quit)
            {
                break;
            }
            else
            {
                CCLOG("runAsyncTaskPool Sleep...");
            	pthread_cond_wait(&s_SleepCondition, &s_SleepMutex);
                continue;
            }
        }
        else
        {
            do
            {
                SBAsyncTask* pAsyncTask = pTasks->at(0);
                
                if (!pAsyncTask->isStarted())
                {
                    pAsyncTask->start();
                }
                else if (pAsyncTask->isFinished())
                {
                    CC_SAFE_RELEASE(pAsyncTask);
                    pTasks->erase(pTasks->begin());
                }
            } while (false);
            
            pthread_mutex_unlock(&s_asyncTasksMutex);
        }
    }
    
    if(s_pAsyncTasks != NULL)
    {
        delete s_pAsyncTasks;
        s_pAsyncTasks = NULL;
        
        pthread_mutex_destroy(&s_asyncTasksMutex);
        pthread_mutex_destroy(&s_SleepMutex);
        pthread_cond_destroy(&s_SleepCondition);
    }
    
    CCLOG("runAsyncTaskPool destroy.");
    
    return 0;
}

SBAsyncTaskPool::SBAsyncTaskPool()
{
    
}

SBAsyncTaskPool::~SBAsyncTaskPool()
{
    need_quit = true;
}

SBAsyncTaskPool* SBAsyncTaskPool::getInstance()
{
    if (!m_pAsyncTaskPool)
    {
        m_pAsyncTaskPool = new SBAsyncTaskPool();
    }
    return m_pAsyncTaskPool;
}

void SBAsyncTaskPool::addAsynTask(SBAsyncTask* pTask)
{
    if (s_pAsyncTasks == NULL)
    {
        s_pAsyncTasks = new vector<SBAsyncTask*>();
        
        pthread_mutex_init(&s_asyncTasksMutex, NULL);
        
        pthread_mutex_init(&s_SleepMutex, NULL);
        pthread_cond_init(&s_SleepCondition, NULL);
        
        pthread_create(&s_asyncTaskThread, NULL, runAsyncTaskPool, NULL);
        
        need_quit = false;
    }
    
    // add async task
    pthread_mutex_lock(&s_asyncTasksMutex);
    s_pAsyncTasks->push_back(pTask);
    pthread_mutex_unlock(&s_asyncTasksMutex);
}

void SBAsyncTaskPool::startRun()
{
    pthread_cond_signal(&s_SleepCondition);
}

bool SBAsyncTaskPool::empty()
{
    if (s_pAsyncTasks)
    {
        return s_pAsyncTasks->empty();
    }
    return true;
}

