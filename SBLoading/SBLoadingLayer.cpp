//
//  SBLoadingLayer.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 8/30/14.
//
//

#include "SBLoadingLayer.h"

SBLoadingLayer::SBLoadingLayer()
{
    m_pAniManager = nullptr;
    m_pAsyncTask.clear();
    
    onLoadStopped = nullptr;
    
    CCLOG("SBLoadingLayer()");
}

SBLoadingLayer::~SBLoadingLayer()
{
    m_pAsyncTask.clear();
    
    CCLOG("~SBLoadingLayer()");
}

void SBLoadingLayer::onEnterTransitionDidFinish()
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    
    Node::onEnterTransitionDidFinish();
    
    loading();
}

void SBLoadingLayer::onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader)
{
    enableSwallow();
}

void SBLoadingLayer::addAsyncTask(SBAsyncTask* pAsyncTask)
{
    pAsyncTask->setAsyncTaskObserver(this, ASYNC_TASK_CALLFUNC(SBLoadingLayer::loadGameResTaskCallBack));
    m_pAsyncTask.push_back(pAsyncTask);
}

void SBLoadingLayer::loading()
{
    SBAsyncTaskPool* pPool = SBAsyncTaskPool::getInstance();
    
    for (int i = 0; i < m_pAsyncTask.size(); i++) {
        SBAsyncTask* pAsyncTask = m_pAsyncTask.at(i);
        pPool->addAsynTask(pAsyncTask);
    }
    this->schedule(schedule_selector(SBLoadingLayer::loadFinishedCheck));
    
    pPool->startRun();
}

void SBLoadingLayer::loadGameResTaskCallBack(SBAsyncTask* pAsyncTask, bool bFinished, void* pData)
{
    if (bFinished)
    {
        for (int i = 0; i < m_pAsyncTask.size(); i++)
        {
            if (pAsyncTask == m_pAsyncTask.at(i)) {
                m_pAsyncTask.erase(m_pAsyncTask.begin() + i);
                break;
            }
        }
    }
}

void SBLoadingLayer::loadFinishedCheck(float t)
{
    if (m_pAsyncTask.empty()) {
        loadFinished();
    }
}

void SBLoadingLayer::loadFinished()
{
    unschedule(schedule_selector(SBLoadingLayer::loadFinishedCheck));
    
    //  load stopped callback
    if (onLoadStopped) { onLoadStopped(this); }
}

