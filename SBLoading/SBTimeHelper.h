//
//  SBTimeHelper.h
//  MyGame01
//
//  Created by LiuHuanMing on 8/30/14.
//
//

#ifndef __MyGame01__SBTimeHelper__
#define __MyGame01__SBTimeHelper__

#include <stdio.h>
#include <time.h>
#include <string>

#define SBTH SBTimeHelper::getTimeHelper()

class SBTimeHelper
{
public:
    static SBTimeHelper* getTimeHelper();
    
    tm* getLocalTime();
    long getLocalTimeTick();    //返回当前时间转换成的秒数
    long getLocalTimeMTick();   //返回当前时间为毫秒
    std::string getStringByTick(long tick); //转换timestamp为string
    long recordTimeStamp();
    
private:
    SBTimeHelper();
    
private:
    static SBTimeHelper* m_pTimeHelper;
    
private:
    int m_iLastTimeStamp;
    
};

#endif /* defined(__MyGame01__SBTimeHelper__) */

