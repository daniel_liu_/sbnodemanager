//
//  SBAsyncTask.h
//  MyGame01
//
//  Created by LiuHuanMing on 8/29/14.
//
//

#ifndef __MyGame01__SBAsyncTask__
#define __MyGame01__SBAsyncTask__

#include "cocos2d.h"
#include "cocos-ext.h"
#include <string>
#include <queue>

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

//  callback
class SBAsyncTask;

typedef void (Ref::*AsyncTaskCallBack)(SBAsyncTask* pAsyncTask, bool bFinished, void* pData);
#define ASYNC_TASK_CALLFUNC(f)  (AsyncTaskCallBack)(&f)

/**
 @brief:    异步任务虚基类，继承者需实现 isFinished方法
 */
class SBAsyncTask : public Ref
{
public:
    SBAsyncTask();
    
    virtual void start();
    virtual bool isFinished() = 0;
    bool isStarted();
    
    void setAsyncTaskObserver(Ref* pObserver,AsyncTaskCallBack pCallBack);
    
private:
    bool m_bStarted;
    
protected:
    Ref*                m_pObserver;
    AsyncTaskCallBack   m_pCallBack;
};

/**
 @brief:    异步加载纹理资源文件
 */
class SBLoadTextureTask : public SBAsyncTask
{
public:
    SBLoadTextureTask();
    ~SBLoadTextureTask();
    virtual void start();
    virtual bool isFinished();
    void addTextrueFile(string file);
    
private:
    int             m_loaded;
    vector<string>  m_textureFiles;
};

/**
 @brief:    异步加载CCBI文件
 */
class SBLoadCCBITask : public SBAsyncTask
{
public:
    SBLoadCCBITask();
    ~SBLoadCCBITask();
    virtual void start();
    virtual bool isFinished();
    void addCCBIFile(string file);
    
private:
    int             m_loaded;
    vector<string>  m_ccbiFiles;
};

/**
 @brief:    异步任务池
 */
class SBAsyncTaskPool
{
public:
    static SBAsyncTaskPool* getInstance();
    
    ~SBAsyncTaskPool();
    
    void addAsynTask(SBAsyncTask* pTask);
    bool empty();
    void clear();
    void startRun();
    
private:
    SBAsyncTaskPool();
    
private:
    static SBAsyncTaskPool* m_pAsyncTaskPool;
};

#endif /* defined(__MyGame01__SBAsyncTask__) */

