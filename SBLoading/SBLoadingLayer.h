//
//  SBLoadingLayer.h
//  MyGame01
//
//  Created by LiuHuanMing on 8/30/14.
//
//

#ifndef __MyGame01__SBLoadingLayer__
#define __MyGame01__SBLoadingLayer__

#include "SBUINode.h"

#include "SBAsyncTask.h"

class SBLoadingLayer : public SBUINode
{
public:
    SBLoadingLayer();
    ~SBLoadingLayer();
    
    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(SBLoadingLayer, create);
    SB_STATIC_CREATENODE_METHOD(SBLoadingLayer, "loadinglayer.ccbi");
    
    virtual void onEnterTransitionDidFinish();
    virtual void onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader);
    
public:
    void addAsyncTask(SBAsyncTask* pAsyncTask);
    
protected:
    void loading();
    void loadGameResTaskCallBack(SBAsyncTask* pAsyncTask,bool bFinished,void* pData);
    void loadFinishedCheck(float t);
    void loadFinished();
    
private:
    CCBAnimationManager*    m_pAniManager;
    vector<SBAsyncTask*>    m_pAsyncTask;
    
public:
    std::function<void(SBLoadingLayer*)> onLoadStopped;     //  on load stopped
    
};

CLASS_NODE_LOADER2(SBLoadingLayer);

#endif /* defined(__MyGame01__SBLoadingLayer__) */

