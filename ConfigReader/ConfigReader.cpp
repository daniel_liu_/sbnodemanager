//
//  ConfigReader.cpp
//  MyGame01
//
//  Created by LiuHuanMing on 8/5/14.
//
//

#include "ConfigReader.h"
#include "Rijndael.h"

#pragma mark - ConfigReader

template <typename T>
Vector<T*> readObjectConfig(const char *cfgFileName, char seprate, bool decrypt)
{
    Vector<T*> vec;
    
    FileUtils *fileUtils = FileUtils::getInstance();
    string fullPath = fileUtils->fullPathForFilename(cfgFileName);
    unsigned char *arrCharBuffer = NULL;
    ssize_t buffSize = 0;
    arrCharBuffer = fileUtils->getFileData(fullPath.c_str(), "r", &buffSize);
    string strContent((char*)arrCharBuffer, buffSize);
    if (decrypt) {
        string desStrContent = decryt(strContent);
        strContent = desStrContent;
    }
    
    string thisLine;
    istringstream fileStringStream(strContent);
    while (getline(fileStringStream, thisLine))
    {
        T* obj = new T();
        obj->autorelease();
        vector<string> substrs = split(thisLine.c_str(), seprate);
        for (int i = 0; i < substrs.size(); i++) {
            obj->updateProperty(i, substrs[i].c_str());
        }
        vec.pushBack(obj);
    }
    
    return vec;
}

template <typename T>
vector<T> readStructConfig(const char *cfgFileName, char seprate, bool decrypt, function<void(T*, int, string)> func)
{
    vector<T> vec;
    
    FileUtils *fileUtils = FileUtils::getInstance();
    string fullPath = fileUtils->fullPathForFilename(cfgFileName);
    unsigned char *arrCharBuffer = NULL;
    ssize_t buffSize = 0;
    arrCharBuffer = fileUtils->getFileData(fullPath.c_str(), "r", &buffSize);
    string strContent((char*)arrCharBuffer, buffSize);
    if (decrypt) {
        string desStrContent = decryt(strContent);
        strContent = desStrContent;
    }
    
    string thisLine;
    istringstream fileStringStream(strContent);
    while (getline(fileStringStream, thisLine))
    {
        T obj;
        vector<string> substrs = split(thisLine.c_str(), seprate);
        for (int i = 0; i < substrs.size(); i++) {
            func(&obj, i, substrs[i].c_str());
        }
        vec.push_back(obj);
    }
    
    return vec;
}

#pragma mark - NewTaskObject

Vector<NewTaskObject *> NewTaskObject::m_pConfigs;

void NewTaskObject::reloadConfigs()
{
    m_pConfigs.clear();
    m_pConfigs = readObjectConfig<NewTaskObject>("newtask.cfg", ',', true);
}

Vector<NewTaskObject *> NewTaskObject::getObjects()
{
    if (m_pConfigs.empty()) {
        reloadConfigs();
    }
    return m_pConfigs;
}

NewTaskObject* NewTaskObject::getObjectAtIndex(int index)
{
    if (index >= m_pConfigs.size()) { return nullptr; }
    return m_pConfigs.at(index);
}

void NewTaskObject::dumpInfo()
{
    Vector<NewTaskObject*> taskObjects = NewTaskObject::getObjects();
    for (int i = 0; i < taskObjects.size(); i++) {
        NewTaskObject *object = taskObjects.at(i);
        CCLOG("task id: %d, desc: %s, reward id: %d", object->getTaskId(), object->getDesc().c_str(), object->getRewardId());
    }
}

#pragma mark - TaskStruct

void TaskSetter(TaskStruct *task, int idx, string str)
{
    if (idx == 0) { task->taskId = atoi(str.c_str()); }
    if (idx == 1) { task->desc = str; }
    if (idx == 2) { task->rewardId = atoi(str.c_str()); }
}

void TaskReloadConfigs()
{
    m_vTasks.clear();
    
    function<void(TaskStruct*, int, string)> func_ = bind(TaskSetter, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    m_vTasks = readStructConfig<TaskStruct>("newtask.txt", ',', false, func_);
}

vector<TaskStruct> TaskGetAll()
{
    if (m_vTasks.empty()) {
        TaskReloadConfigs();
    }
    return m_vTasks;
}

TaskStruct TaskGetAtIndex(int index)
{
    if (index >= m_vTasks.size()) {
        CCASSERT(false, "Out of Size in m_vTasks");
    }
    return m_vTasks.at(index);
}

void TaskDumpInfo()
{
    vector<TaskStruct> tasks = TaskGetAll();
    for (int i = 0; i < tasks.size(); i++) {
        TaskStruct task_ = TaskGetAtIndex(i);
        CCLOG("taskstruct - id: %d, desc: %s, rewardid: %d", task_.taskId, task_.desc.c_str(), task_.rewardId);
    }
}

