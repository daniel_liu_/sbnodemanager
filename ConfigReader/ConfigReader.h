//
//  ConfigReader.h
//  MyGame01
//
//  Created by LiuHuanMing on 8/5/14.
//
//

#ifndef __MyGame01__ConfigReader__
#define __MyGame01__ConfigReader__

#include "cocos2d.h"
#include "cocos-ext.h"
#include <stdlib.h>
#include <string>

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

/**
 * Config Reader
 */
template <typename T>
Vector<T*> readObjectConfig(const char *cfgFileName, char seprate, bool decrypt = false);

template <typename T>
vector<T> readStructConfig(const char *cfgFileName, char seprate, bool decrypt, function<void(T&, int, string)> func);

/**
 * Utility
 */
std::vector<std::string> split(const std::string &s, char delim);
char* CharVectorToString(std::vector<char>* pVectorBuff);

/**
 * Basic Config Object
 */
class ConfigObject: public Ref {
    virtual void updateProperty(int idx, const char *str) = 0;
};

//  NewTaskObject
class NewTaskObject : public ConfigObject
{
private:
    static Vector<NewTaskObject *> m_pConfigs;
    
public:
    NewTaskObject() { CCLOG("NewTaskObject init!!!"); };
    ~NewTaskObject() { CCLOG("NewTaskObject dealloc!!"); };
    
    CC_SYNTHESIZE_PASS_BY_REF(int, m_taskId, TaskId);
    CC_SYNTHESIZE_PASS_BY_REF(string, m_pDesc, Desc);
    CC_SYNTHESIZE_PASS_BY_REF(int, m_rewardId, RewardId);
    
public:
    void updateProperty(int idx, const char *str) {
        if (idx == 0) { setTaskId(atoi(str)); }
        if (idx == 1) { setDesc(str); }
        if (idx == 2) { setRewardId(atoi(str)); }
    }
    
public:
    static void reloadConfigs();
    static Vector<NewTaskObject *> getObjects();
    static NewTaskObject* getObjectAtIndex(int index);
    
    static void dumpInfo();
};

//  TaskStruct
struct TaskStruct {
    int taskId;
    string desc;
    int rewardId;
};
static vector<TaskStruct> m_vTasks;

void TaskSetter(TaskStruct *task, int idx, string str);
void TaskReloadConfigs();
vector<TaskStruct> TaskGetAll();
TaskStruct TaskGetAtIndex(int index);
void TaskDumpInfo();

#endif /* defined(__MyGame01__ConfigReader__) */

